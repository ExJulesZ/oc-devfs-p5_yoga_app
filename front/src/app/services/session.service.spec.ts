import { TestBed } from '@angular/core/testing';
import { expect } from '@jest/globals';

import { SessionService } from './session.service';
import { SessionInformation } from '../interfaces/sessionInformation.interface';

describe('SessionService', () => {
  let service: SessionService;

  const mockUser: SessionInformation = {
    token: "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ5b2dhQHN0dWRpby5jb20iLCJpYXQiOjE2OTUzMzQxMDcsImV4cCI6MTY5NTQyMDUwN30.KQKl2dUUR5O5xSgpUH6xBkgzuhLan_lEc7x_FOCpn-6siT7IUGdjt3Etg9HAiUXEywSP6fie9AyrKOCsuyAgqw",
    type: "Bearer",
    id: 1,
    username: "jeandupont@mail.com",
    firstName: "Jean",
    lastName: "Dupont",
    admin: false
  }

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SessionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return false', () => {
    expect(service.isLogged).toBeFalsy();
  });
  it('should return undefined value', () => {
    expect(service.sessionInformation).toBeUndefined();
  });

  it('should return something defined', async () => {
    const data = await service.$isLogged();
    expect(data).toBeDefined();
  });

  it('should return true', () => {
    service.logIn(mockUser);
    expect(service.isLogged).toBeTruthy();
  });
  it('should return something defined', () => {
    service.logIn(mockUser);
    expect(service.sessionInformation).toBeDefined();
  });
  it('should return 1', () => {
    service.logIn(mockUser);
    expect(service.sessionInformation?.id).toBe(1);
  });
  it('should return "jeandupont@mail.com"', () => {
    service.logIn(mockUser);
    expect(service.sessionInformation?.username).toBe("jeandupont@mail.com");
  });

  it('should return false', () => {
    service.logIn(mockUser);
    service.logOut();
    expect(service.isLogged).toBeFalsy();
  });
  it('should return undefined value', () => {
    service.logIn(mockUser);
    service.logOut();
    expect(service.sessionInformation).toBeUndefined();
  });

});
