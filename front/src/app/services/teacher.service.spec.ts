import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { expect } from '@jest/globals';

import { TeacherService } from './teacher.service';

describe('TeacherService', () => {
  let service: TeacherService;
  let httpClient: HttpClient;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [TeacherService]
    });
    
    service = TestBed.inject(TeacherService);
    httpClient = TestBed.inject(HttpClient);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should send correct request to get all teachers', async () => {
    const request = jest.spyOn(httpClient, 'get');
    service.all().subscribe();
    expect(request).toHaveBeenCalledWith('api/teacher');
  });
  it('should return something defined for getting all teachers', async () => {
    const data = await service.all();
    expect(data).toBeDefined();
  });

  it('should send correct request to get teacher 1', async () => {
    const request = jest.spyOn(httpClient, 'get');
    service.detail("1").subscribe();
    expect(request).toHaveBeenCalledWith('api/teacher/1');
  });
  it('should return something defined for getting teacher 1', async () => {
    const data = await service.detail("1");
    expect(data).toBeDefined();
  });

});
