import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { expect } from '@jest/globals';

import { UserService } from './user.service';

describe('UserService', () => {
  let service: UserService;
  let httpClient: HttpClient;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [UserService]
    });

    service = TestBed.inject(UserService);
    httpClient = TestBed.inject(HttpClient);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should send correct request to get user 1', async () => {
    const request = jest.spyOn(httpClient, 'get');
    service.getById("1").subscribe();
    expect(request).toHaveBeenCalledWith('api/user/1');
  });
  it('should return something defined for getting user 1', async () => {
    const data = await service.getById("1");
    expect(data).toBeDefined();
  });

  it('should send correct request to delete user 1', async () => {
    const request = jest.spyOn(httpClient, 'delete');
    service.delete("1").subscribe();
    expect(request).toHaveBeenCalledWith('api/user/1');
  });
  it('should return something defined for deleting user 1', async () => {
    const data = await service.delete("1");
    expect(data).toBeDefined();
  });

});
