import { HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { expect } from '@jest/globals';
import { SessionService } from '../../../../services/session.service';
import { SessionApiService } from '../../services/session-api.service';

import { FormComponent } from './form.component';

describe('FormComponent', () => {
  let component: FormComponent;
  let fixture: ComponentFixture<FormComponent>;

  const mockSessionService = {
    sessionInformation: {
      token: "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ5b2dhQHN0dWRpby5jb20iLCJpYXQiOjE2OTUzMzQxMDcsImV4cCI6MTY5NTQyMDUwN30.KQKl2dUUR5O5xSgpUH6xBkgzuhLan_lEc7x_FOCpn-6siT7IUGdjt3Etg9HAiUXEywSP6fie9AyrKOCsuyAgqw",
      type: "Bearer",
      id: 1,
      username: "jeandupont@mail.com",
      firstName: "Jean",
      lastName: "Dupont",
      admin: true
    }
  } 

  beforeEach(async () => {
    await TestBed.configureTestingModule({

      imports: [
        RouterTestingModule,
        HttpClientModule,
        MatCardModule,
        MatIconModule,
        MatFormFieldModule,
        MatInputModule,
        ReactiveFormsModule, 
        MatSnackBarModule,
        MatSelectModule,
        BrowserAnimationsModule
      ],
      providers: [
        { provide: SessionService, useValue: mockSessionService },
        SessionApiService
      ],
      declarations: [FormComponent]
    })
      .compileComponents();

    fixture = TestBed.createComponent(FormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should not contain "update" (pathname)', () => {
    expect(global.window.location.pathname).not.toContain('update');
  });

  it('should return false (onUpdate)', () => {
    expect(component.onUpdate).toBeFalsy();
  });

  it('should be defined (sessionForm)', () => {
    expect(component.sessionForm).toBeDefined();
  });

  it('should check that the form is valid', () => {
    component.sessionForm?.setValue({name:"Test Session 1",date:new Date("2023-10-05"),teacher_id:4,description:"Test de première session de yoga."});
    expect(component.sessionForm?.valid).toBeTruthy();
  });

  it('should check that the form is not valid (empty name)', () => {
    component.sessionForm?.setValue({name:"",date:new Date("2023-10-05"),teacher_id:4,description:"Test de première session de yoga."});
    expect(component.sessionForm?.valid).toBeFalsy();
  });

  it('should check that the form is not valid (empty date)', () => {
    component.sessionForm?.setValue({name:"Test Session 1",date:"",teacher_id:4,description:"Test de première session de yoga."});
    expect(component.sessionForm?.valid).toBeFalsy();
  });

  it('should check that the form is not valid (empty teacher_id)', () => {
    component.sessionForm?.setValue({name:"Test Session 1",date:new Date("2023-10-05"),teacher_id:"",description:"Test de première session de yoga."});
    expect(component.sessionForm?.valid).toBeFalsy();
  });

  it('should check that the form is not valid (empty description)', () => {
    component.sessionForm?.setValue({name:"Test Session 1",date:new Date("2023-10-05"),teacher_id:4,description:""});
    expect(component.sessionForm?.valid).toBeFalsy();
  });
  it('should check that the form is not valid (invalid description)', () => {
    component.sessionForm?.setValue({name:"Test Session 1",date:new Date("2023-10-05"),teacher_id:4,description:"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer sit amet lobortis magna. Cras dignissim dignissim pulvinar. Praesent tempor nisi non neque consectetur, cursus viverra lacus consectetur. Nulla et augue ipsum. Maecenas pretium ultricies metus, a condimentum dui. Morbi sit amet nunc nunc. Phasellus tempus ex sit amet pretium fermentum. Aliquam in imperdiet orci. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Fusce ut vestibulum orci. Etiam eu semper enim. Phasellus malesuada ornare cursus. Sed non massa auctor, pulvinar justo quis, rutrum elit. In tempus dapibus justo et hendrerit. Fusce consectetur nunc eu nulla pretium imperdiet. Cras enim nisl, viverra ut ex id, rutrum consectetur sem. Pellentesque quis varius nunc, a viverra elit. Donec pellentesque imperdiet accumsan. Nulla non ante risus. Proin laoreet ex dolor, non consectetur nisi bibendum vel. Aliquam lacus libero, lobortis ac pharetra eget, dapibus eu sapien. Morbi iaculis et sem et pellentesque. Duis blandit vel est a porttitor. Nullam et risus fermentum, varius ipsum at, volutpat lorem. Nulla nec metus ut lorem accumsan facilisis. Maecenas eu erat pretium, mattis elit vitae, accumsan dolor. In in pharetra leo. Nullam vel magna et est porta vehicula. Etiam molestie metus ac dapibus congue. Etiam at sem tellus. Vivamus ullamcorper massa nec leo vestibulum, sed suscipit dui cursus. Donec tempor velit metus, eget bibendum sapien mollis sit amet. Maecenas fermentum nunc non magna dapibus egestas. In dapibus, ante eu ornare mollis, sem nulla rutrum mauris, sed suscipit dui urna ut est. Maecenas ac elit malesuada libero dapibus dictum a nec orci. Vestibulum semper turpis sollicitudin, tristique lorem vitae, viverra turpis. In fermentum posuere est, eget bibendum nibh pharetra non. Cras pulvinar lorem nec vulputate vestibulum. Sed faucibus sem enim, nec aliquet libero laoreet viverra. Duis nec mauris ullamcorper, consectetur felis ut, pharetra lacus porta ante."});
    expect(component.sessionForm?.valid).toBeFalsy();
  });
  
});
