import { HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { RouterTestingModule, } from '@angular/router/testing';
import { expect } from '@jest/globals'; 
import { SessionService } from '../../../../services/session.service';

import { DetailComponent } from './detail.component';


describe('DetailComponent', () => {
  let component: DetailComponent;
  let fixture: ComponentFixture<DetailComponent>; 
  let service: SessionService;

  const mockSessionService = {
    sessionInformation: {
      token: "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ5b2dhQHN0dWRpby5jb20iLCJpYXQiOjE2OTUzMzQxMDcsImV4cCI6MTY5NTQyMDUwN30.KQKl2dUUR5O5xSgpUH6xBkgzuhLan_lEc7x_FOCpn-6siT7IUGdjt3Etg9HAiUXEywSP6fie9AyrKOCsuyAgqw",
      type: "Bearer",
      id: 1,
      username: "jeandupont@mail.com",
      firstName: "Jean",
      lastName: "Dupont",
      admin: true
    }
  }

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        HttpClientModule,
        MatSnackBarModule,
        ReactiveFormsModule
      ],
      declarations: [DetailComponent], 
      providers: [{ provide: SessionService, useValue: mockSessionService }],
    })
      .compileComponents();
      service = TestBed.inject(SessionService);
    fixture = TestBed.createComponent(DetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    component.sessionId = "3";
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should be undefined (session)', () => {
    expect(component.session).toBeUndefined();
  });
  it('should be undefined (teacher)', () => {
    expect(component.teacher).toBeUndefined();
  });

  it('should return false (isParticipate)', () => {
    expect(component.isParticipate).toBeFalsy();
  });

  it('should return true (isAdmin)', () => {
    expect(component.isAdmin).toBeTruthy();
  });

  it('should return "3" (sessionId)', () => {
    expect(component.sessionId).toEqual("3");
  });

  it('should return "1" (userId)', () => {
    expect(component.userId).toEqual("1");
  });

});

