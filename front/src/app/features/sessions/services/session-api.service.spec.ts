import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { expect } from '@jest/globals';

import { SessionApiService } from './session-api.service';
import { Session } from '../interfaces/session.interface';

describe('SessionsService', () => {
  let service: SessionApiService;
  let httpClient: HttpClient;

  const mockSessionCreate: Session = {
    id: 2,
    name: "Session 2",
    description: "La deuxième session de yoga",
    date: new Date("2023-12-15"),
    teacher_id: 5,
    users: [],
    createdAt: new Date(),
    updatedAt: new Date()
  };

  const mockSessionUpdate: Session = {
    id: 2,
    name: "Session n°2",
    description: "C'est la deuxième session de yoga.",
    date: new Date("2023-12-16"),
    teacher_id: 4,
    users: [],
    createdAt: new Date(),
    updatedAt: new Date()
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [SessionApiService]
    });

    service = TestBed.inject(SessionApiService);
    httpClient = TestBed.inject(HttpClient);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should send correct request to get all sessions', async () => {
    const request = jest.spyOn(httpClient, 'get');
    service.all().subscribe();
    expect(request).toHaveBeenCalledWith('api/session');
  });
  it('should return something defined for getting all sessions', async () => {
    const data = await service.all();
    expect(data).toBeDefined();
  });

  it('should send correct request to get session 1', async () => {
    const request = jest.spyOn(httpClient, 'get');
    service.detail("1").subscribe();
    expect(request).toHaveBeenCalledWith('api/session/1');
  });
  it('should return something defined for getting session 1', async () => {
    const data = await service.detail("1");
    expect(data).toBeDefined();
  });

  it('should send correct request to delete session 1', async () => {
    const request = jest.spyOn(httpClient, 'delete');
    service.delete("1").subscribe();
    expect(request).toHaveBeenCalledWith('api/session/1');
  });
  it('should return something defined for deleting session 1', async () => {
    const data = await service.delete("1");
    expect(data).toBeDefined();
  });

  it('should send correct request to create session 2', async () => {
    const request = jest.spyOn(httpClient, 'post');
    service.create(mockSessionCreate).subscribe();
    expect(request).toHaveBeenCalledWith('api/session',mockSessionCreate);
  });
  it('should return something defined for creating session 2', async () => {
    const data = await service.create(mockSessionCreate);
    expect(data).toBeDefined();
  });

  it('should send correct request to update session 2', async () => {
    const request = jest.spyOn(httpClient, 'put');
    service.update("2", mockSessionUpdate).subscribe();
    expect(request).toHaveBeenCalledWith('api/session/2',mockSessionUpdate);
  });
  it('should return something defined for updating session 2', async () => {
    const data = await service.update("2", mockSessionUpdate);
    expect(data).toBeDefined();
  });

  it('should send correct request to participate in session 2', async () => {
    const request = jest.spyOn(httpClient, 'post');
    service.participate("2", "1").subscribe();
    expect(request).toHaveBeenCalledWith('api/session/2/participate/1', null);
  });
  it('should return something defined for participating in session 2', async () => {
    const data = await service.participate("2", "1");
    expect(data).toBeDefined();
  });

  it('should send correct request to unparticipate in session 2', async () => {
    const request = jest.spyOn(httpClient, 'delete');
    service.unParticipate("2", "1").subscribe();
    expect(request).toHaveBeenCalledWith('api/session/2/participate/1');
  });
  it('should return something defined for unparticipating in session 2', async () => {
    const data = await service.unParticipate("2", "1");
    expect(data).toBeDefined();
  });

});
