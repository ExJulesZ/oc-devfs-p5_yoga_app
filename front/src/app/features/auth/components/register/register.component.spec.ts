import { HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { expect } from '@jest/globals';

import { RegisterComponent } from './register.component';

describe('RegisterComponent', () => {
  let component: RegisterComponent;
  let fixture: ComponentFixture<RegisterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RegisterComponent],
      imports: [
        BrowserAnimationsModule,
        HttpClientModule,
        ReactiveFormsModule,  
        MatCardModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule
      ]
    })
      .compileComponents();

    fixture = TestBed.createComponent(RegisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    
    component.form.setValue({email:"jeandupont@mail.com",firstName:"Jean",lastName:"Dupont",password:"Password!123"});
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should return "jeandupont@mail.com" (email)', () => {
    expect(component.form.value.email).toBe("jeandupont@mail.com");
  });
  it('should return "Jean" (firstName)', () => {
    expect(component.form.value.firstName).toBe("Jean");
  });
  it('should return "Dupont" (lastName)', () => {
    expect(component.form.value.lastName).toBe("Dupont");
  });
  it('should return "Password!123" (password)', () => {
    expect(component.form.value.password).toBe("Password!123");
  });

  it('should check that the form is valid', () => {
    expect(component.form.valid).toBeTruthy();
  });

  it('should check that the form is not valid (empty email)', () => {
    component.form.setValue({email:"",firstName:"Jean",lastName:"Dupont",password:"Password!123"});
    expect(component.form.valid).toBeFalsy();
  });
  it('should check that the form is not valid (invalid email)', () => {
    component.form.setValue({email:"jeandupontmail.com",firstName:"Jean",lastName:"Dupont",password:"Password!123"});
    expect(component.form.valid).toBeFalsy();
  });

  it('should check that the form is not valid (empty firstName)', () => {
    component.form.setValue({email:"jeandupont@mail.com",firstName:"",lastName:"Dupont",password:"Password!123"});
    expect(component.form.valid).toBeFalsy();
  });
  it('should check that the form is not valid (invalid firstName 1)', () => {
    component.form.setValue({email:"jeandupont@mail.com",firstName:"Je",lastName:"Dupont",password:"Password!123"});
    expect(component.form.valid).toBeFalsy();
  });
  it('should check that the form is not valid (invalid firstName 2)', () => {
    component.form.setValue({email:"jeandupont@mail.com",firstName:"Jeanjeanjeanjeanjeanjean",lastName:"Dupont",password:"Password!123"});
    expect(component.form.valid).toBeFalsy();
  });

  it('should check that the form is not valid (empty lastName)', () => {
    component.form.setValue({email:"jeandupont@mail.com",firstName:"Jean",lastName:"",password:"Password!123"});
    expect(component.form.valid).toBeFalsy();
  });
  it('should check that the form is not valid (invalid lastName 1)', () => {
    component.form.setValue({email:"jeandupont@mail.com",firstName:"Jean",lastName:"Du",password:"Password!123"});
    expect(component.form.valid).toBeFalsy();
  });
  it('should check that the form is not valid (invalid lastName 2)', () => {
    component.form.setValue({email:"jeandupont@mail.com",firstName:"Jean",lastName:"Dupontdupontdupontdupont",password:"Password!123"});
    expect(component.form.valid).toBeFalsy();
  });

  it('should check that the form is not valid (empty password)', () => {
    component.form.setValue({email:"jeandupont@mail.com",firstName:"Jean",lastName:"Dupont",password:""});
    expect(component.form.valid).toBeFalsy();
  });
  it('should check that the form is not valid (invalid password 1)', () => {
    component.form.setValue({email:"jeandupont@mail.com",firstName:"Jean",lastName:"Dupont",password:"Pa"});
    expect(component.form.valid).toBeFalsy();
  });
  it('should check that the form is not valid (invalid password 2)', () => {
    component.form.setValue({email:"jeandupont@mail.com",firstName:"Jean",lastName:"Dupont",password:"Passwordpasswordpasswordpasswordpassword!123"});
    expect(component.form.valid).toBeFalsy();
  });

  it('should not return any error when register', () => {
    component.submit();
    expect(component.onError).toBeFalsy();
  });

});
