import { HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { expect } from '@jest/globals';
import { SessionService } from '../../../../services/session.service';

import { LoginComponent } from './login.component';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [LoginComponent],
      providers: [SessionService],
      imports: [
        RouterTestingModule,
        BrowserAnimationsModule,
        HttpClientModule,
        MatCardModule,
        MatIconModule,
        MatFormFieldModule,
        MatInputModule,
        ReactiveFormsModule]
    })
      .compileComponents();
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    
    component.form.setValue({email:"yoga@studio.com",password:"test!1234"});
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should return "yoga@studio.com" (email)', () => {
    expect(component.form.value.email).toBe("yoga@studio.com");
  });
  it('should return "test!1234" (password)', () => {
    expect(component.form.value.password).toBe("test!1234");
  });

  it('should check that the form is valid', () => {
    expect(component.form.valid).toBeTruthy();
  });

  it('should check that the form is not valid (empty email)', () => {
    component.form.setValue({email:"",password:"test!1234"});
    expect(component.form.valid).toBeFalsy();
  });
  it('should check that the form is not valid (invalid email)', () => {
    component.form.setValue({email:"yogastudiocom",password:"test!1234"});
    expect(component.form.valid).toBeFalsy();
  });

  it('should check that the form is not valid (empty password)', () => {
    component.form.setValue({email:"yoga@studio.com",password:""});
    expect(component.form.valid).toBeFalsy();
  });
  it('should check that the form is not valid (invalid password)', () => {
    component.form.setValue({email:"yoga@studio.com",password:"te"});
    expect(component.form.valid).toBeFalsy();
  });

  it('should not return any error when login', () => {
    component.submit();
    expect(component.onError).toBeFalsy();
  });

});
