import { Mock } from "./mock";
const mock = new Mock();

describe('Form update spec', () => {

    function login() {
        cy.visit('/login');

        cy.intercept('POST', '/api/auth/login', { body: mock.mockAdmin }).as('login');
        cy.intercept('GET', '/api/session', { body: [mock.mockSession] }).as('getAllSessions');
        cy.intercept('GET', '/api/session/'+mock.mockSession.id, { body: mock.mockSession }).as('getSession');
        cy.intercept('GET', '/api/teacher', { body: [mock.mockTeacher, mock.mockTeacherSecond] }).as('allTeachers');

        cy.get('input[formControlName=email]').type(mock.mockAdmin.email);
        cy.get('input[formControlName=password]').type(mock.mockAdmin.password);

        cy.get('.login-form').first().submit();

        cy.get('button').eq(2).click();
    }

    it('should navigate to correct page', () => {
        login();
        cy.url().should('include', '/update');
    });

    it('should navigate to correct page on click "Save" button', () => {
        login();

        cy.intercept('PUT', '/api/session/'+mock.mockEditedSession.id, { body: mock.mockEditedSession }).as('updateSession');

        cy.get('input[formControlName=name]').clear().type(mock.mockEditedSession.name);
        let date = mock.mockEditedSession.date;
        let dateString = date.getFullYear()+'-'+(date.getMonth()+1)+'-'+date.getDate();
        cy.get('input[formControlName=date]').clear().type(dateString);
        cy.get('mat-select[formControlName=teacher_id]').click().get('mat-option').eq(mock.mockEditedSession.teacher_id-1).click();
        cy.get('textarea[formControlName=description]').clear().type(mock.mockEditedSession.description);

        cy.get('form').first().submit();

        cy.url().should('include', '/sessions');
    });
});