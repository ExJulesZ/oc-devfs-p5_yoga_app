import { Mock } from "./mock";
const mock = new Mock();

describe('Detail spec', () => {

    function login(admin:boolean) {
        cy.visit('/login');

        if(admin) cy.intercept('POST', '/api/auth/login', { body: mock.mockAdmin }).as('login');
        else cy.intercept('POST', '/api/auth/login', { body: mock.mockUser }).as('login');

        cy.intercept('GET', '/api/session', { body: [mock.mockSession] }).as('getAllSessions');
        cy.intercept('GET', '/api/teacher/'+mock.mockTeacher.id, { body: mock.mockTeacher }).as('getTeacher');
        cy.intercept('GET', '/api/session/'+mock.mockSession.id, { body: mock.mockSession }).as('getSession');

        if(admin){
            cy.get('input[formControlName=email]').type(mock.mockAdmin.email);
            cy.get('input[formControlName=password]').type(mock.mockAdmin.password);
        } else {
            cy.get('input[formControlName=email]').type(mock.mockUser.email);
            cy.get('input[formControlName=password]').type(mock.mockUser.password);
        }

        cy.get('.login-form').first().submit();

        if(admin) cy.get('button').eq(1).click();
        else cy.get('button').eq(0).click();
    }

    it('should navigate to correct page when admin', () => {
        login(true);
        cy.url().should('include', '/detail');
    });

    it('should display left arrow and "Delete" buttons when admin', () => {
        login(true);
        cy.get('button').should('have.length', 2).eq(1).should('have.text', 'deleteDelete');
    });

    it('should navigate to correct page on click left arrow button', () => {
        login(true);
        cy.get('button').eq(0).click();
        cy.url().should('include', '/sessions');
    });

    it('should navigate to correct page on click "Delete" button', () => {
        login(true);
        cy.intercept('DELETE', '/api/session/'+mock.mockSession.id, {}).as('deleteSession');
        cy.get('button').eq(1).click();
        cy.url().should('include', '/sessions');
    });

    it('should navigate to correct page when not admin', () => {
        login(false);
        cy.url().should('include', '/detail');
    });

    it('should display left arrow and "Participate" buttons when not admin', () => {
        login(false);
        cy.get('button').should('have.length', 2).eq(1).should('have.text', 'person_addParticipate');
    });
});