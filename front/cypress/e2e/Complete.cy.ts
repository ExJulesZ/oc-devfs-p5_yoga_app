import './register.cy';
import './login.cy';
import './list.cy';
import './me.cy';
import './detail.cy';
import './formUpdate.cy';
import './formCreate.cy';
import './notFound.cy';