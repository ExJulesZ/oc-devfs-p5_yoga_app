import { Mock } from "./mock";
const mock = new Mock();

describe('Me spec', () => {

    function login(admin:boolean) {
        cy.visit('/login');

        if(admin){
            cy.intercept('POST', '/api/auth/login', { body: mock.mockAdmin }).as('login');
            cy.intercept('GET', '/api/user/'+mock.mockAdmin.id, { body: mock.mockAdmin }).as('getUser');
        } else {
            cy.intercept('POST', '/api/auth/login', { body: mock.mockUser }).as('login');
            cy.intercept('GET', '/api/user/'+mock.mockUser.id, { body: mock.mockUser }).as('getUser');
        }
        cy.intercept('GET', '/api/session', { body: [mock.mockSession] }).as('getAllSessions');

        if(admin){
            cy.get('input[formControlName=email]').type(mock.mockAdmin.email);
            cy.get('input[formControlName=password]').type(mock.mockAdmin.password);
        } else {
            cy.get('input[formControlName=email]').type(mock.mockUser.email);
            cy.get('input[formControlName=password]').type(mock.mockUser.password);
        }

        cy.get('.login-form').first().submit();

        cy.get('span.link').eq(1).click();
    }

    it('should navigate to correct page when admin', () => {
        login(true);
        cy.url().should('include', '/me');
    });

    it('should display only left arrow button when admin', () => {
        login(true);
        cy.get('button').should('have.length', 1).first().should('have.text', 'arrow_back');
    });

    it('should navigate to correct page on click "Sessions" link', () => {
        login(true);
        cy.get('span.link').eq(0).click();
        cy.url().should('include', '/sessions');
    });

    it('should navigate to correct page on click left arrow button', () => {
        login(true);
        cy.get('button').eq(0).click();
        cy.url().should('include', '/sessions');
    });

    it('should navigate to correct page when admin', () => {
        login(false);
        cy.url().should('include', '/me');
    });

    it('should display left arrow and "Delete" buttons when not admin', () => {
        login(false);
        cy.get('button').should('have.length', 2);
    });

    it('should navigate to correct page on click "Delete" button', () => {
        login(false);
        cy.intercept('DELETE', '/api/user/'+mock.mockUser.id, { body: mock.mockUser }).as('deleteUser');
        cy.get('button').eq(1).click();
        cy.url().should('include', '/');
    });
});