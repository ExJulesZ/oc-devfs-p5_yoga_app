import { Mock } from "./mock";
const mock = new Mock();

describe('Form create spec', () => {

    function login() {
        cy.visit('/login');

        cy.intercept('POST', '/api/auth/login', { body: mock.mockAdmin }).as('login');
        cy.intercept('GET', '/api/session', { body: [mock.mockSession] }).as('getAllSessions');
        cy.intercept('GET', '/api/teacher', { body: [mock.mockTeacher, mock.mockTeacherSecond] }).as('allTeachers');

        cy.get('input[formControlName=email]').type(mock.mockAdmin.email);
        cy.get('input[formControlName=password]').type(mock.mockAdmin.password);

        cy.get('.login-form').first().submit();

        cy.get('button').eq(0).click();
    }

    it('should navigate to correct page', () => {
        login();
        cy.url().should('include', '/create');
    });

    it('should navigate to correct page on click "Save" button', () => {
        login();

        cy.intercept('POST', '/api/session', { body: mock.mockNewSession }).as('createSession');

        cy.get('input[formControlName=name]').type(mock.mockNewSession.name);
        let date = mock.mockNewSession.date;
        let dateString = date.getFullYear()+'-'+(date.getMonth()+1)+'-'+date.getDate();
        cy.get('input[formControlName=date]').type(dateString);
        cy.get('mat-select[formControlName=teacher_id]').click().get('mat-option').eq(mock.mockNewSession.teacher_id-1).click();
        cy.get('textarea[formControlName=description]').type(mock.mockNewSession.description);

        cy.get('form').first().submit();

        cy.url().should('include', '/sessions');
    });
});