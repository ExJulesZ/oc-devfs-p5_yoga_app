import { Mock } from "./mock";
const mock = new Mock();

describe('List spec', () => {

    beforeEach( () => {
        cy.visit('/login');

        cy.intercept('POST', '/api/auth/login', { body: mock.mockAdmin }).as('login');
        cy.intercept('GET', '/api/session', { body: [mock.mockSession] }).as('getAllSessions');

        cy.get('input[formControlName=email]').type("yoga@studio.com");
        cy.get('input[formControlName=password]').type("test!1234");

        cy.get('.login-form').first().submit(); //login admin user
    });

    it('should navigate to correct page when admin', () => {
        cy.url().should('include', '/sessions');
    });

    it('should display only "Detail" button when not admin', () => {
        cy.get('span.link').eq(2).click(); //logout

        cy.visit('/login');

        cy.intercept('POST', '/api/auth/login', { body: { admin: false } }).as('login');
        cy.intercept('GET', '/api/session', { body: [mock.mockSession] }).as('getAllSessions');

        cy.get('input[formControlName=email]').type("jules.grelet@live.fr");
        cy.get('input[formControlName=password]').type("PasswordJG19");

        cy.get('.login-form').first().submit(); //login not admin user

        cy.get('button').should('have.length', 1).first().should('have.text', 'searchDetail');
    });

    it('should display "Create", "Detail" and "Edit" buttons when admin', () => {
        cy.get('button').should('have.length', 3).first().should('have.text', 'addCreate');
    });

    it('should navigate to correct page on click "Account" link', () => {
        cy.intercept('GET', '/api/user/'+mock.mockAdmin.id, { body: mock.mockAdmin }).as('getUser');
        cy.get('span.link').eq(1).click(); //Account link
        cy.url().should('include', '/me');
    });

    it('should navigate to correct page on click "Create" button', () => {
        cy.intercept('GET', '/api/teacher', { body: [mock.mockTeacher, mock.mockTeacherSecond] }).as('allTeachers');
        cy.get('button').eq(0).click(); //Create button
        cy.url().should('include', '/create');
    });

    it('should navigate to correct page on click "Detail" button', () => {
        cy.intercept('GET', '/api/teacher/'+mock.mockTeacher.id, { body: mock.mockTeacher }).as('getTeacher');
        cy.intercept('GET', '/api/session/'+mock.mockSession.id, { body: mock.mockSession }).as('getSession');
        cy.get('button').eq(1).click(); //Detail button
        cy.url().should('include', '/detail');
    });

    it('should navigate to correct page on click "Update" button', () => {
        cy.intercept('GET', '/api/session/'+mock.mockSession.id, { body: mock.mockSession }).as('getSession');
        cy.intercept('GET', '/api/teacher', { body: [mock.mockTeacher, mock.mockTeacherSecond] }).as('allTeachers');
        cy.get('button').eq(2).click(); //Update button
        cy.url().should('include', '/update');
    });
});