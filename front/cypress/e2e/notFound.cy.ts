describe('Not found spec', () => {

    it('should navigate to 404 page', () => {
        cy.visit('/hello');
        cy.url().should('include', '/404');
    });
});