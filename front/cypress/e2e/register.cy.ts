describe('Register spec', () => {

    it('should register successfull', () => {
        cy.visit('/register');
    
        cy.intercept('POST', '/api/auth/register', {}).as('register');

        cy.get('input[formControlName=firstName]').type("Jean");
        cy.get('input[formControlName=lastName]').type("Dupont");
        cy.get('input[formControlName=email]').type("jeandupont@mail.com");
        cy.get('input[formControlName=password]').type("Password!123");
        
        cy.get('.register-form').first().submit();

        cy.url().should('include', '/login');
    });
  });