export class Mock{

    mockTeacher = {
        id: 1,
        lastName: "DELAHAYE",
        firstName: "Margot",
        createdAt: new Date(),
        updatedAt: new Date()
    };

    mockTeacherSecond = {
        id: 2,
        lastName: "THIERCELIN",
        firstName: "Hélène",
        createdAt: new Date(),
        updatedAt: new Date()
    };

    mockSession = {
        id: 5,
        name: "Session de Test",
        description: "C'est une session de Test.",
        date: new Date("2023-11-05"),
        teacher_id: 1,
        users: [],
        createdAt: new Date(),
        updatedAt: new Date()
    };

    mockAdmin = {
        id: 1,
        email: "yoga@studio.com",
        lastName: "admin",
        firstName: "admin",
        admin: true,
        password: "test!1234",
        createdAt: new Date(),
        updatedAt: new Date()
    };

    mockUser = {
        id: 2,
        email: "jeandupont@mail.com",
        lastName: "Dupont",
        firstName: "Jean",
        admin: false,
        password: "Password!123",
        createdAt: new Date(),
        updatedAt: new Date()
    };

    mockNewSession = {
        id: 6,
        name: "Nouvelle session",
        description: "C'est une nouvelle session.",
        date: new Date("2023-12-15"),
        teacher_id: 1,
        users: [],
        createdAt: new Date(),
        updatedAt: new Date()
    }

    mockEditedSession = {
        id: 5,
        name: "Nouvelle session modifiée",
        description: "C'est une nouvelle session modifiée.",
        date: new Date("2023-11-26"),
        teacher_id: 2,
        users: [],
        createdAt: new Date(),
        updatedAt: new Date()
    }

}