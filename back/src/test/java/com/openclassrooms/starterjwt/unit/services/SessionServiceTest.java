package com.openclassrooms.starterjwt.unit.services;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.util.Assert;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;

import com.openclassrooms.starterjwt.exception.BadRequestException;
import com.openclassrooms.starterjwt.exception.NotFoundException;
import com.openclassrooms.starterjwt.models.Session;
import com.openclassrooms.starterjwt.models.Teacher;
import com.openclassrooms.starterjwt.models.User;
import com.openclassrooms.starterjwt.repository.SessionRepository;
import com.openclassrooms.starterjwt.repository.UserRepository;
import com.openclassrooms.starterjwt.services.SessionService;

@ExtendWith(MockitoExtension.class)
public class SessionServiceTest {

    @Mock
    private SessionRepository sessionRepository;
    @Mock
    private UserRepository userRepository;

    private SessionService sessionService;

    private LocalDateTime now;
    private Teacher teacher;
    private Session session;

    @BeforeEach
    public void init() {
        this.sessionService = new SessionService(this.sessionRepository,this.userRepository);
        this.now = LocalDateTime.now();
        this.teacher = new Teacher(5L,"Thiercelin","Hélène",this.now,this.now);
        this.session = new Session(1L,"Session 1",new Date(),"Session n°1",this.teacher,new ArrayList<>(),this.now,this.now);
    }

    @Test
    public void testCreate() {
        // GIVEN
        when(this.sessionRepository.save(this.session)).thenReturn(this.session);

        // WHEN
        final Session res = this.sessionService.create(this.session);

        // THEN
        Assert.state(res.equals(this.session), "Created session must be equal to this.session");
        Assert.state(res.getId().equals(this.session.getId()), "Created session id must be equal to this.session id");
    }

    @Test
    public void testDelete() {
        // GIVEN
        final Long id = this.session.getId();
        doNothing().when(this.sessionRepository).deleteById(id);

        // WHEN
        this.sessionService.delete(id);

        // THEN
        verify(this.sessionRepository,times(1)).deleteById(id);
    }

    @Test
    public void testFindAll() {
        // GIVEN
        Session session2 = new Session(2L,"Session 2",new Date(),"Session n°2",this.teacher,new ArrayList<>(),this.now,this.now);
        List<Session> sessions = new ArrayList<>();
        sessions.add(this.session);
        sessions.add(session2);
        when(this.sessionRepository.findAll()).thenReturn(sessions);

        // WHEN
        final List<Session> res = this.sessionService.findAll();

        // THEN
        Assert.isTrue(res.size()==2, "Sessions list size must be equal to 2");
        Assert.state(res.get(0).equals(this.session), "Sessions list first element must be equal to this.session");
        Assert.state(res.get(1).equals(session2), "Sessions list second element must be equal to session2");
        Assert.state(res.get(0).getId().equals(this.session.getId()), "Sessions list first element id must be equal to this.session id");
        Assert.state(res.get(1).getId().equals(session2.getId()), "Sessions list second element id must be equal to session2 id");
    }

    @Test
    public void testGetById() {
        // GIVEN
        final Long id = this.session.getId();
        when(this.sessionRepository.findById(id)).thenReturn(Optional.of(this.session));

        // WHEN
        final Session res = this.sessionService.getById(id);

        // THEN
        Assert.state(res.equals(this.session), "Found session must be equal to this.session");
        Assert.state(res.getId().equals(this.session.getId()), "Found session id must be equal to this.session id");
    }

    @Test
    public void testGetByIdNull() {
        // GIVEN
        final Long id = this.session.getId();
        when(this.sessionRepository.findById(id)).thenReturn(Optional.empty());

        // WHEN
        final Session res = this.sessionService.getById(id);

        // THEN
        Assert.isNull(res, "Found session must be null");
    }

    @Test
    public void testUpdate() {
        // GIVEN
        when(this.sessionRepository.save(this.session)).thenReturn(this.session);

        // WHEN
        final Session res = this.sessionService.update(this.session.getId(),this.session);

        // THEN
        Assert.state(res.equals(this.session), "Updated session must be equal to this.session");
        Assert.state(res.getId().equals(this.session.getId()), "Updated session id must be equal to this.session id");
    }

    @Test
    public void testParticipate() {
        // GIVEN
        final Long sessionId = this.session.getId();
        final Long userId = 1L;
        User user = new User(userId,"jules.grelet@live.fr","Grelet","Jules","passwd000",true,now,now);
        when(this.sessionRepository.findById(sessionId)).thenReturn(Optional.of(this.session));
        when(this.userRepository.findById(userId)).thenReturn(Optional.of(user));

        // WHEN
        this.sessionService.participate(sessionId,userId);

        // THEN
        verify(this.sessionRepository,times(1)).save(this.session);
    }

    @Test
    public void testParticipateNotFoundExceptionSessionNull() {
        // GIVEN
        final Long sessionId = this.session.getId();
        final Long userId = 1L;
        User user = new User(userId,"jules.grelet@live.fr","Grelet","Jules","passwd000",true,now,now);
        when(this.sessionRepository.findById(sessionId)).thenReturn(Optional.empty());
        when(this.userRepository.findById(userId)).thenReturn(Optional.of(user));

        // WHEN / THEN
        assertThrows(NotFoundException.class, () -> {
            this.sessionService.participate(sessionId,userId);
        });
    }

    @Test
    public void testParticipateNotFoundExceptionUserNull() {
        // GIVEN
        final Long sessionId = this.session.getId();
        final Long userId = 1L;
        when(this.sessionRepository.findById(sessionId)).thenReturn(Optional.of(this.session));
        when(this.userRepository.findById(userId)).thenReturn(Optional.empty());

        // WHEN / THEN
        assertThrows(NotFoundException.class, () -> {
            this.sessionService.participate(sessionId,userId);
        });
    }

    @Test
    public void testParticipateBadRequestException() {
        // GIVEN
        final Long sessionId = this.session.getId();
        final Long userId = 1L;
        User user = new User(userId,"jules.grelet@live.fr","Grelet","Jules","passwd000",true,now,now);
        this.session.getUsers().add(user);
        when(this.sessionRepository.findById(sessionId)).thenReturn(Optional.of(this.session));
        when(this.userRepository.findById(userId)).thenReturn(Optional.of(user));

        // WHEN / THEN
        assertThrows(BadRequestException.class, () -> {
            this.sessionService.participate(sessionId, userId);
        });
    }

    @Test
    public void testNoLongerParticipate() {
        // GIVEN
        final Long sessionId = this.session.getId();
        final Long userId = 1L;
        User user = new User(userId,"jules.grelet@live.fr","Grelet","Jules","passwd000",true,now,now);
        this.session.getUsers().add(user);
        when(this.sessionRepository.findById(sessionId)).thenReturn(Optional.of(this.session));

        // WHEN
        this.sessionService.noLongerParticipate(sessionId,userId);

        // THEN
        verify(this.sessionRepository,times(1)).save(this.session);
    }

    @Test
    public void testNoLongerParticipateNotFoundException() {
        // GIVEN
        final Long sessionId = this.session.getId();
        final Long userId = 1L;
        when(this.sessionRepository.findById(userId)).thenReturn(Optional.empty());

        // WHEN / THEN
        assertThrows(NotFoundException.class, () -> {
            this.sessionService.noLongerParticipate(sessionId,userId);
        });
    }

    @Test
    public void testNoLongerParticipateBadRequestException() {
        // GIVEN
        final Long sessionId = this.session.getId();
        final Long userId = 1L;
        when(this.sessionRepository.findById(userId)).thenReturn(Optional.of(this.session));

        // WHEN / THEN
        assertThrows(BadRequestException.class, () -> {
            this.sessionService.noLongerParticipate(sessionId,userId);
        });
    }
}
