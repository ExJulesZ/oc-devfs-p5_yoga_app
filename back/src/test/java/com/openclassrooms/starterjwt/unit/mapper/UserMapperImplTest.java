package com.openclassrooms.starterjwt.unit.mapper;

import org.springframework.util.Assert;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.openclassrooms.starterjwt.dto.UserDto;
import com.openclassrooms.starterjwt.mapper.UserMapperImpl;
import com.openclassrooms.starterjwt.models.User;

public class UserMapperImplTest {

    private UserMapperImpl userMapperImpl;
    private User user;
    private UserDto dto;

    @BeforeEach
    public void setUserMapperImpl() {
        this.userMapperImpl = new UserMapperImpl();
        LocalDateTime now = LocalDateTime.now();
        this.user = new User(1L,"yoga@studio.com","Admin","Admin","Admin1234",true,now,now);
        this.dto = new UserDto(1L,"yoga@studio.com","Admin","Admin",true,"Admin1234",now,now);
    }

    @Test
    public void testToEntity() {
        Assert.state(this.userMapperImpl.toEntity(this.dto).equals(this.user), "UserDto must match with User");
    }

    @Test
    public void testToEntityNull() {
        Assert.isNull(this.userMapperImpl.toEntity((UserDto) null), "UserDto is null, it must be return null");
    }

    @Test
    public void testToEntityList() {
        List<UserDto> dtoList = new ArrayList<>();
        dtoList.add(this.dto);
        List<User> userList = new ArrayList<>();
        userList.add(this.user);
        Assert.state(this.userMapperImpl.toEntity(dtoList).equals(userList), "UserDto list must match with User list");
    }

    @Test
    public void testToEntityListNull() {
        Assert.isNull(this.userMapperImpl.toEntity((List<UserDto>) null), "UserDto list is null, it must be return null");
    }

    @Test
    public void testToDto() {
        Assert.state(this.userMapperImpl.toDto(this.user).equals(this.dto), "User must match with UserDto");
    }

    @Test
    public void testToDtoNull() {
        Assert.isNull(this.userMapperImpl.toDto((User) null), "User is null, it must be return null");
    }

    @Test
    public void testToDtoList() {
        List<User> userList = new ArrayList<>();
        userList.add(this.user);
        List<UserDto> dtoList = new ArrayList<>();
        dtoList.add(this.dto);
        Assert.state(this.userMapperImpl.toDto(userList).equals(dtoList), "User list must match with UserDto list");
    }

    @Test
    public void testToDtoListNull() {
        Assert.isNull(this.userMapperImpl.toDto((List<User>) null), "User list is null, it must be return null");
    }
}
