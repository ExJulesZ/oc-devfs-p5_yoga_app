package com.openclassrooms.starterjwt.unit.models;

import org.springframework.util.Assert;

import java.time.LocalDateTime;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.openclassrooms.starterjwt.models.User;

public class UserTest {

    private LocalDateTime now;

    private User user;
    private User user2;

    @BeforeEach
    public void setUser() {
        this.now = LocalDateTime.now();
        this.user = new User(10L, "jules.grelet@live.fr","Grelet","Jules","Azerty!28",false,this.now,this.now);
        this.user2 = new User("jules.grelet@live.fr","Grelet","Jules","Azerty!28",false);
    }

    @Test
    public void testId() {
        Assert.state(this.user.getId().equals(10L), "Id must be equal to 10 (Long)");
    }

    @Test
    public void testId2() {
        Assert.isNull(this.user2.getId(), "Id of user2 must be null");
    }

    @Test
    public void testEmail() {
        Assert.state(this.user.getEmail().equals("jules.grelet@live.fr"), "Email must be equal to \"jules.grelet@live.fr\"");
    }

    @Test
    public void testLastName() {
        Assert.state(this.user.getLastName().equals("Grelet"), "Last name must be equal to \"Grelet\"");
    }

    @Test
    public void testFirstName() {
        Assert.state(this.user.getFirstName().equals("Jules"), "First name must be equal to \"Jules\"");
    }

    @Test
    public void testPassword() {
        Assert.state(this.user.getPassword().equals("Azerty!28"), "Password must be equal to \"Azerty!28\"");
    }

    @Test
    public void testAdmin() {
        Assert.isTrue(!this.user.isAdmin(), "Admin must be equal to false");
    }

    @Test
    public void testCreatedAt() {
        Assert.state(this.user.getCreatedAt().toString().equals(this.now.toString()), "CreatedAt must be equal to this.now");
    }

    @Test
    public void testUpdatedAt() {
        Assert.state(this.user.getUpdatedAt().toString().equals(this.now.toString()), "UpdatedAt must be equal to this.now");
    }

    @Test
    public void testCreatedAt2() {
        Assert.isNull(this.user2.getCreatedAt(), "CreatedAt of user2 must be equal to this.now");
    }

    @Test
    public void testUpdatedAt2() {
        Assert.isNull(this.user2.getUpdatedAt(), "UpdatedAt of user2 must be equal to this.now");
    }

    @Test
    public void testNewId() {
        this.user.setId(11L);
        Assert.state(this.user.getId().equals(11L), "Now, id must be equal to 11 (Long)");
    }

    @Test
    public void testNewEmail() {
        this.user.setEmail("louis.breton@live.fr");
        Assert.state(this.user.getEmail().equals("louis.breton@live.fr"), "Now, email must be equal to \"louis.breton@live.fr\"");
    }

    @Test
    public void testNewLastName() {
        this.user.setLastName("Breton");
        Assert.state(this.user.getLastName().equals("Breton"), "Now, last name must be equal to \"Breton\"");
    }

    @Test
    public void testNewFirstName() {
        this.user.setFirstName("Louis");
        Assert.state(this.user.getFirstName().equals("Louis"), "Now, first name must be equal to \"Louis\"");
    }

    @Test
    public void testNewPassword() {
        this.user.setPassword("Toto-555");
        Assert.state(this.user.getPassword().equals("Toto-555"), "Now, password must be equal to \"Toto-555\"");
    }

    @Test
    public void testNewAdmin() {
        this.user.setAdmin(true);
        Assert.isTrue(this.user.isAdmin(), "Now, admin must be equal to true");
    }

    @Test
    public void testNewCreatedAt() {
        this.user.setCreatedAt(this.now);
        Assert.state(this.user.getCreatedAt().toString().equals(this.now.toString()), "Now, createdAt must be equal to this.now (another date time because current)");
    }

    @Test
    public void testNewUpdatedAt() {
        this.user.setUpdatedAt(this.now);
        Assert.state(this.user.getUpdatedAt().toString().equals(this.now.toString()), "Now, updatedAt must be equal to this.now (another date time because current)");
    }

    @Test
    public void testEquals1() {
        Assert.isTrue(this.user.equals(this.user), "user object must be equal to himself");
    }
    @Test
    public void testEquals2() {
        User user3 = new User(12L,"gabriel.dupuy@live.fr","Dupuy","Gabriel","Gab0000",false,this.now,this.now);
        Assert.isTrue(!this.user.equals(user3), "user object mustn't be equal to an another object (user3)");
    }
    @Test
    public void testEquals3() {
        Assert.isTrue(!this.user.equals(null), "user object mustn't be equal to a null object");
    }

    @Test
    public void testHashCode() {
        Assert.isTrue(this.user.hashCode()==69, "HashCode of user object must be equal to 69");
    }

    @Test
    public void testToString() {
        String now = this.now.toString();
        Assert.state(this.user.toString().equals("User(id=10, email=jules.grelet@live.fr, lastName=Grelet, firstName=Jules, password=Azerty!28, admin=false, createdAt="+now+", updatedAt="+now+")"), "user object toString must be equal to \"User(id=10, email=jules.grelet@live.fr, lastName=Grelet, firstName=Jules, password=Azerty!28, admin=false, createdAt="+now+", updatedAt="+now+")\"");
    }

    @Test
    public void testBuilderToString() {
        Assert.state(User.builder().toString().equals("User.UserBuilder(id=null, email=null, lastName=null, firstName=null, password=null, admin=false, createdAt=null, updatedAt=null)"), "User Builder object toString must be equal to \"User.UserBuilder(id=null, email=null, lastName=null, firstName=null, password=null, admin=false, createdAt=null, updatedAt=null)\"");
    }

    @Test
    public void testBuilder() {
        User builtUser = User.builder().id(14L).email("juste.leblanc@live.fr").lastName("Leblanc").firstName("Juste").password("Avril1998").admin(true).createdAt(this.now).updatedAt(this.now).build();
        Assert.state(builtUser.getId().equals(14L), "Built User id must be equal to 14 (Long)");
        Assert.state(builtUser.getEmail().equals("juste.leblanc@live.fr"), "Built User email must be equal to \"juste.leblanc@live.fr\"");
        Assert.state(builtUser.getLastName().equals("Leblanc"), "Built User last name must be equal to \"Leblanc\"");
        Assert.state(builtUser.getFirstName().equals("Juste"), "Built User first name must be equal to \"Juste\"");
        Assert.state(builtUser.getPassword().equals("Avril1998"), "Built User password must be equal to \"Avril1998\"");
        Assert.isTrue(builtUser.isAdmin(), "Built User admin must be equal to true");
        Assert.state(builtUser.getCreatedAt().equals(this.now), "Built User updatedAt must be equal to this.now");
        Assert.state(builtUser.getUpdatedAt().equals(this.now), "Built User updatedAt must be equal to this.now");
    }
}
