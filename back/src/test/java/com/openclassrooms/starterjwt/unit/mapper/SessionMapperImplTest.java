package com.openclassrooms.starterjwt.unit.mapper;

import org.springframework.util.Assert;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.openclassrooms.starterjwt.dto.SessionDto;
import com.openclassrooms.starterjwt.mapper.SessionMapperImpl;
import com.openclassrooms.starterjwt.models.Session;
import com.openclassrooms.starterjwt.models.Teacher;

public class SessionMapperImplTest {

    private SessionMapperImpl sessionMapperImpl;

    @BeforeEach
    public void setSessionMapperImpl() {
        this.sessionMapperImpl = new SessionMapperImpl();
    }

    @Test
    public void testToEntity() {
        SessionDto dto = new SessionDto();
        Session session = new Session();
        Assert.state(this.sessionMapperImpl.toEntity(dto).equals(session), "SessionDto must match with Session");
    }

    @Test
    public void testToEntityNull() {
        Assert.isNull(this.sessionMapperImpl.toEntity((SessionDto) null), "SessionDto is null, it must be return null");
    }

    @Test
    public void testToEntityList() {
        List<SessionDto> dtoList = new ArrayList<>();
        dtoList.add(new SessionDto());
        List<Session> sessionList = new ArrayList<>();
        sessionList.add(new Session());
        Assert.state(this.sessionMapperImpl.toEntity(dtoList).equals(sessionList), "SessionDto list must match with Session list");
    }

    @Test
    public void testToEntityListNull() {
        Assert.isNull(this.sessionMapperImpl.toEntity((List<SessionDto>) null), "SessionDto list is null, it must be return null");
    }

    @Test
    public void testToDto() {
        Session session = new Session();
        SessionDto dto = new SessionDto();
        dto.setUsers(new ArrayList<>());
        Assert.state(this.sessionMapperImpl.toDto(session).equals(dto), "Session must match with SessionDto");
    }

    @Test
    public void testToDtoData() {
        Date date = new Date();
        LocalDateTime now = LocalDateTime.now();
        Teacher teacher = new Teacher(5L,"Thiercelin","Hélène",now,now);
        Session session = new Session(1L,"Session 1",date,"Session n°1",teacher,new ArrayList<>(),now,now);
        SessionDto dto = new SessionDto(1L,"Session 1",date,5L,"Session n°1",new ArrayList<>(),now,now);
        Assert.state(this.sessionMapperImpl.toDto(session).equals(dto), "Session must match with SessionDto");
    }

    @Test
    public void testToDtoNull() {
        Assert.isNull(this.sessionMapperImpl.toDto((Session) null), "Session is null, it must be return null");
    }

    @Test
    public void testToDtoList() {
        List<Session> sessionList = new ArrayList<>();
        sessionList.add(new Session());
        List<SessionDto> dtoList = new ArrayList<>();
        dtoList.add(new SessionDto());
        dtoList.get(0).setUsers(new ArrayList<>());
        Assert.state(this.sessionMapperImpl.toDto(sessionList).equals(dtoList), "Session list must match with SessionDto list");
    }

    @Test
    public void testToDtoListNull() {
        Assert.isNull(this.sessionMapperImpl.toDto((List<Session>) null), "Session list is null, it must be return null");
    }
}
