package com.openclassrooms.starterjwt.unit.payload;

import org.springframework.util.Assert;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.openclassrooms.starterjwt.payload.response.JwtResponse;

public class JwtResponseTest {

    private JwtResponse jwtResponse;

    @BeforeEach
    public void setJwt() {
        this.jwtResponse = new JwtResponse("123456789", 5L,"jeandupont@mail.com","Jean","Dupont",false);
    }

    @Test
    public void testToken() {
        Assert.state(this.jwtResponse.getToken().equals("123456789"), "Token must be equal to \"123456789\"");
    }

    @Test
    public void testType() {
        Assert.state(this.jwtResponse.getType().equals("Bearer"), "Type must be equal to \"Bearer\"");
    }

    @Test
    public void testId() {
        Assert.state(this.jwtResponse.getId().equals(5L), "Id must be equal to 5 (Long)");
    }

    @Test
    public void testUsername() {
        Assert.state(this.jwtResponse.getUsername().equals("jeandupont@mail.com"), "Username must be equal to \"jeandupont@mail.com\"");
    }

    @Test
    public void testFirstName() {
        Assert.state(this.jwtResponse.getFirstName().equals("Jean"), "First name must be equal to \"Jean\"");
    }

    @Test
    public void testLastName() {
        Assert.state(this.jwtResponse.getLastName().equals("Dupont"), "Last name must be equal to \"Dupont\"");
    }

    @Test
    public void testAdmin() {
        Assert.state(this.jwtResponse.getAdmin().equals(false), "Admin must be equal to false");
    }

    @Test
    public void testNewToken() {
        this.jwtResponse.setToken("987654321");
        Assert.state(this.jwtResponse.getToken().equals("987654321"), "Now, token must be equal to \"987654321\"");
    }

    @Test
    public void testNewType() {
        this.jwtResponse.setType("Test");
        Assert.state(this.jwtResponse.getType().equals("Test"), "Now, type must be equal to \"Test\"");
    }

    @Test
    public void testNewId() {
        this.jwtResponse.setId(7L);
        Assert.state(this.jwtResponse.getId().equals(7L), "Now, id must be equal to 7 (Long)");
    }

    @Test
    public void testNewUsername() {
        this.jwtResponse.setUsername("jules.grelet@live.fr");
        Assert.state(this.jwtResponse.getUsername().equals("jules.grelet@live.fr"), "Now, username must be equal to \"jules.grelet@live.fr\"");
    }

    @Test
    public void testNewFirstName() {
        this.jwtResponse.setFirstName("Jules");
        Assert.state(this.jwtResponse.getFirstName().equals("Jules"), "Now, first name must be equal to \"Jules\"");
    }

    @Test
    public void testNewLastName() {
        this.jwtResponse.setLastName("Grelet");
        Assert.state(this.jwtResponse.getLastName().equals("Grelet"), "Now, last name must be equal to \"Grelet\"");
    }

    @Test
    public void testNewAdmin() {
        this.jwtResponse.setAdmin(true);
        Assert.state(this.jwtResponse.getAdmin().equals(true), "Now, admin must be equal to true");
    }
}
