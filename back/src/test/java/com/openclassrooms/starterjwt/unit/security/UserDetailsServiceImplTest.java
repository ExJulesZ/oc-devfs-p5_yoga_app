package com.openclassrooms.starterjwt.unit.security;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.util.Assert;

import java.time.LocalDateTime;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import com.openclassrooms.starterjwt.models.User;
import com.openclassrooms.starterjwt.repository.UserRepository;
import com.openclassrooms.starterjwt.security.services.UserDetailsServiceImpl;

@ExtendWith(MockitoExtension.class)
public class UserDetailsServiceImplTest {

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private UserDetailsServiceImpl userDetailsServiceImpl;

    @BeforeEach
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testLoadUserByUsername() {
        // GIVEN
        LocalDateTime now = LocalDateTime.now();
        User user = new User(1L,"jules.grelet@live.fr","Grelet","Jules","passwd000",true,now,now);
        when(this.userRepository.findByEmail("jules.grelet@live.fr")).thenReturn(Optional.of(user));

        // WHEN
        final UserDetails userDetails = this.userDetailsServiceImpl.loadUserByUsername("jules.grelet@live.fr");

        // THEN
        Assert.state(userDetails.getUsername().equals(user.getEmail()), "UserDetails username must be equal to user email");
        Assert.state(userDetails.getPassword().equals(user.getPassword()), "UserDetails password must be equal to user password");
    }

    @Test
    public void testLoadUserByUsernameException() {
        // GIVEN
        when(this.userRepository.findByEmail("jules.grelet@live.fr")).thenReturn(Optional.empty());

        // WHEN
        Exception exception = assertThrows(UsernameNotFoundException.class, () -> {
            this.userDetailsServiceImpl.loadUserByUsername("jules.grelet@live.fr");
        });
        String expectedMessage = "User Not Found with email: jules.grelet@live.fr";
        String actualMessage = exception.getMessage();

        // THEN
        assertTrue(actualMessage.contains(expectedMessage));
    }
}
