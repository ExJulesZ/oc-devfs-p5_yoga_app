package com.openclassrooms.starterjwt.unit.mapper;

import org.springframework.util.Assert;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.openclassrooms.starterjwt.dto.TeacherDto;
import com.openclassrooms.starterjwt.mapper.TeacherMapperImpl;
import com.openclassrooms.starterjwt.models.Teacher;

public class TeacherMapperImplTest {

    private TeacherMapperImpl teacherMapperImpl;

    @BeforeEach
    public void setTeacherMapperImpl() {
        this.teacherMapperImpl = new TeacherMapperImpl();
    }

    @Test
    public void testToEntity() {
        TeacherDto dto = new TeacherDto();
        Teacher teacher = new Teacher();
        Assert.state(this.teacherMapperImpl.toEntity(dto).equals(teacher), "SessionDto must match with Session");
    }

    @Test
    public void testToEntityNull() {
        Assert.isNull(this.teacherMapperImpl.toEntity((TeacherDto) null), "SessionDto is null, it must be return null");
    }

    @Test
    public void testToEntityList() {
        List<TeacherDto> dtoList = new ArrayList<>();
        dtoList.add(new TeacherDto());
        List<Teacher> teacherList = new ArrayList<>();
        teacherList.add(new Teacher());
        Assert.state(this.teacherMapperImpl.toEntity(dtoList).equals(teacherList), "SessionDto list must match with Session list");
    }

    @Test
    public void testToEntityListNull() {
        Assert.isNull(this.teacherMapperImpl.toEntity((List<TeacherDto>) null), "SessionDto list is null, it must be return null");
    }

    @Test
    public void testToDto() {
        Teacher teacher = new Teacher();
        TeacherDto dto = new TeacherDto();
        Assert.state(this.teacherMapperImpl.toDto(teacher).equals(dto), "Session must match with SessionDto");
    }

    @Test
    public void testToDtoData() {
        LocalDateTime now = LocalDateTime.now();
        Teacher teacher = new Teacher(1L,"Delahaye","Margot",now,now);
        TeacherDto dto = new TeacherDto(1L,"Delahaye","Margot",now,now);
        Assert.state(this.teacherMapperImpl.toDto(teacher).equals(dto), "Session must match with SessionDto");
    }

    @Test
    public void testToDtoNull() {
        Assert.isNull(this.teacherMapperImpl.toDto((Teacher) null), "Session is null, it must be return null");
    }

    @Test
    public void testToDtoList() {
        List<Teacher> teacherList = new ArrayList<>();
        teacherList.add(new Teacher());
        List<TeacherDto> dtoList = new ArrayList<>();
        dtoList.add(new TeacherDto());
        Assert.state(this.teacherMapperImpl.toDto(teacherList).equals(dtoList), "Session list must match with SessionDto list");
    }

    @Test
    public void testToDtoListNull() {
        Assert.isNull(this.teacherMapperImpl.toDto((List<Teacher>) null), "Session list is null, it must be return null");
    }
}
