package com.openclassrooms.starterjwt.unit.payload;

import org.springframework.util.Assert;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.openclassrooms.starterjwt.payload.response.MessageResponse;

public class MessageResponseTest {

    private MessageResponse messageResponse;

    @BeforeEach
    public void setMessage() {
        this.messageResponse = new MessageResponse("Error: Email is already taken!");
    }

    @Test
    public void testMessage() {
        Assert.state(this.messageResponse.getMessage().equals("Error: Email is already taken!"), "Message must be equal to \"Error: Email is already taken!\"");
    }

    @Test
    public void testNewMessage() {
        this.messageResponse.setMessage("User registered successfully!");
        Assert.state(this.messageResponse.getMessage().equals("User registered successfully!"), "Now, message must be equal to \"User registered successfully!\"");
    }
}
