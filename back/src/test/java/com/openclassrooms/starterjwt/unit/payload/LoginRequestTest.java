package com.openclassrooms.starterjwt.unit.payload;

import org.springframework.util.Assert;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.openclassrooms.starterjwt.payload.request.LoginRequest;

public class LoginRequestTest {

    private LoginRequest loginRequest;

    @BeforeEach
    public void setLogin() {
        this.loginRequest = new LoginRequest();
        this.loginRequest.setEmail("jeandupont@mail.com");
		this.loginRequest.setPassword("Password!123");
    }

    @Test
    public void testEmail() {
        Assert.state(this.loginRequest.getEmail().equals("jeandupont@mail.com"), "Email must be equal to \"jeandupont@mail.com\"");
    }

    @Test
    public void testPassword() {
        Assert.state(this.loginRequest.getPassword().equals("Password!123"), "Password must be equal to \"Password!123\"");
    }

    @Test
    public void testNewEmail() {
        this.loginRequest.setEmail("jules.grelet@live.fr");
        Assert.state(this.loginRequest.getEmail().equals("jules.grelet@live.fr"), "Now, email must be equal to \"jules.grelet@live.fr\"");
    }

    @Test
    public void testNewPassword() {
        this.loginRequest.setPassword("Azerty!28");
        Assert.state(this.loginRequest.getPassword().equals("Azerty!28"), "Now, password must be equal to \"Azerty!28\"");
    }
}
