package com.openclassrooms.starterjwt.unit.models;

import org.springframework.util.Assert;

import java.time.LocalDateTime;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.openclassrooms.starterjwt.models.Teacher;

public class TeacherTest {

    private LocalDateTime now;

    private Teacher teacher;

    @BeforeEach
    public void setTeacher() {
        this.now = LocalDateTime.now();
        this.teacher = new Teacher(14L,"Leroy","Chloé",this.now,this.now);
    }

    @Test
    public void testId() {
        Assert.state(this.teacher.getId().equals(14L), "Id must be equal to 14 (Long)");
    }

    @Test
    public void testLastName() {
        Assert.state(this.teacher.getLastName().equals("Leroy"), "Last name must be equal to \"Leroy\"");
    }

    @Test
    public void testFirstName() {
        Assert.state(this.teacher.getFirstName().equals("Chloé"), "First name must be equal to \"Chloé\"");
    }

    @Test
    public void testCreatedAt() {
        Assert.state(this.teacher.getCreatedAt().toString().equals(this.now.toString()), "CreatedAt must be equal to this.now");
    }

    @Test
    public void testUpdatedAt() {
        Assert.state(this.teacher.getUpdatedAt().toString().equals(this.now.toString()), "UpdatedAt must be equal to this.now");
    }

    @Test
    public void testNewId() {
        this.teacher.setId(15L);
        Assert.state(this.teacher.getId().equals(15L), "Now, id must be equal to 15 (Long)");
    }

    @Test
    public void testNewLastName() {
        this.teacher.setLastName("Duval");
        Assert.state(this.teacher.getLastName().equals("Duval"), "Now, last name must be equal to \"Duval\"");
    }

    @Test
    public void testNewFirstName() {
        this.teacher.setFirstName("Rose");
        Assert.state(this.teacher.getFirstName().equals("Rose"), "Now, first name must be equal to \"Rose\"");
    }

    @Test
    public void testNewCreatedAt() {
        this.teacher.setCreatedAt(this.now);
        Assert.state(this.teacher.getCreatedAt().toString().equals(this.now.toString()), "Now, createdAt must be equal to this.now (another date time because current)");
    }

    @Test
    public void testNewUpdatedAt() {
        this.teacher.setUpdatedAt(this.now);
        Assert.state(this.teacher.getUpdatedAt().toString().equals(this.now.toString()), "Now, updatedAt must be equal to this.now (another date time because current)");
    }

    @Test
    public void testEquals1() {
        Assert.isTrue(this.teacher.equals(this.teacher), "teacher object must be equal to himself");
    }
    @Test
    public void testEquals2() {
        Teacher teacher2 = new Teacher(16L,"Blanchard","Jade",this.now,this.now);
        Assert.isTrue(!this.teacher.equals(teacher2), "teacher object mustn't be equal to an another object (teacher2)");
    }
    @Test
    public void testEquals3() {
        Assert.isTrue(!this.teacher.equals(null), "teacher object mustn't be equal to a null object");
    }

    @Test
    public void testHashCode() {
        Assert.isTrue(this.teacher.hashCode()==73, "HashCode of teacher object must be equal to 73");
    }

    @Test
    public void testToString() {
        String now = this.now.toString();
        Assert.state(this.teacher.toString().equals("Teacher(id=14, lastName=Leroy, firstName=Chloé, createdAt="+now+", updatedAt="+now+")"), "teacher object toString must be equal to \"Teacher(id=14, lastName=Leroy, firstName=Chloé, createdAt="+now+", updatedAt="+now+")\"");
    }

    @Test
    public void testBuilderToString() {
        Assert.state(Teacher.builder().toString().equals("Teacher.TeacherBuilder(id=null, lastName=null, firstName=null, createdAt=null, updatedAt=null)"), "Teacher Builder object toString must be equal to \"Teacher.TeacherBuilder(id=null, lastName=null, firstName=null, createdAt=null, updatedAt=null)\"");
    }

    @Test
    public void testBuilderNull() {
        Teacher builtTeacher = Teacher.builder().build();
        Assert.isNull(builtTeacher.getId(), "Built Teacher id must be null");
        Assert.isNull(builtTeacher.getLastName(), "Built Teacher last name must be null");
        Assert.isNull(builtTeacher.getFirstName(), "Built Teacher first name must be null");
        Assert.isNull(builtTeacher.getCreatedAt(), "Built Teacher createdAt must be null");
        Assert.isNull(builtTeacher.getUpdatedAt(), "Built Teacher updatedAt must be null");
    }

    @Test
    public void testBuilder() {
        Teacher builtTeacher = Teacher.builder().id(17L).lastName("Dufour").firstName("Mia").createdAt(this.now).updatedAt(this.now).build();
        Assert.state(builtTeacher.getId().equals(17L), "Built Teacher id must be equal to 17 (Long)");
        Assert.state(builtTeacher.getLastName().equals("Dufour"), "Built Teacher last name must be equal to \"Dufour\"");
        Assert.state(builtTeacher.getFirstName().equals("Mia"), "Built Teacher first name must be equal to \"Mia\"");
        Assert.state(builtTeacher.getCreatedAt().equals(this.now), "Built Teacher updatedAt must be equal to this.now");
        Assert.state(builtTeacher.getUpdatedAt().equals(this.now), "Built Teacher updatedAt must be equal to this.now");
    }
}
