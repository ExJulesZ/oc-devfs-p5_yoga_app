package com.openclassrooms.starterjwt.unit.security;

import org.springframework.util.Assert;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.openclassrooms.starterjwt.security.services.UserDetailsImpl;
import com.openclassrooms.starterjwt.models.User;

public class UserDetailsImplTest {

    private UserDetailsImpl userDetailsImpl;

    @BeforeEach
    public void setUserDetailsImpl() {
        this.userDetailsImpl = new UserDetailsImpl(20L, "lucas.buisson@live.fr","Lucas","Buisson",false,"00password00");
    }

    @Test
    public void testId() {
        Assert.state(this.userDetailsImpl.getId().equals(20L), "Id must be equal to 20 (Long)");
    }

    @Test
    public void testUsername() {
        Assert.state(this.userDetailsImpl.getUsername().equals("lucas.buisson@live.fr"), "Username must be equal to \"lucas.buisson@live.fr\"");
    }

    @Test
    public void testFirstName() {
        Assert.state(this.userDetailsImpl.getFirstName().equals("Lucas"), "First name must be equal to \"Lucas\"");
    }

    @Test
    public void testLastName() {
        Assert.state(this.userDetailsImpl.getLastName().equals("Buisson"), "Last name must be equal to \"Buisson\"");
    }

    @Test
    public void testAdmin() {
        Assert.isTrue(!this.userDetailsImpl.getAdmin(), "Admin must be equal to false");
    }

    @Test
    public void testPassword() {
        Assert.state(this.userDetailsImpl.getPassword().equals("00password00"), "Password must be equal to \"00password00\"");
    }

    @Test
    public void testAuthorities() {
        Assert.isTrue(this.userDetailsImpl.getAuthorities().size()==0, "Authorities size must be equal to 0");
    }

    @Test
    public void testAccountNonExpired() {
        Assert.isTrue(this.userDetailsImpl.isAccountNonExpired(), "AccountNonExpired must be equal to true");
    }

    @Test
    public void testAccountNonLocked() {
        Assert.isTrue(this.userDetailsImpl.isAccountNonLocked(), "AccountNonLocked must be equal to true");
    }

    @Test
    public void testCredentialsNonExpired() {
        Assert.isTrue(this.userDetailsImpl.isCredentialsNonExpired(), "CredentialsNonExpired must be equal to true");
    }

    @Test
    public void testEnabled() {
        Assert.isTrue(this.userDetailsImpl.isEnabled(), "Enabled must be equal to true");
    }

    @Test
    public void testEquals1() {
        Assert.isTrue(this.userDetailsImpl.equals(this.userDetailsImpl), "userDetailsImpl object must be equal to himself");
    }
    @Test
    public void testEquals2() {
        UserDetailsImpl userDetailsImpl2 = new UserDetailsImpl(21L, "paul.lesage@live.fr","Paul","Lesage",true,"qwerty123");
        Assert.isTrue(!this.userDetailsImpl.equals(userDetailsImpl2), "userDetailsImpl object mustn't be equal to an another object (userDetailsImpl2)");
    }
    @Test
    public void testEquals3() {
        Assert.isTrue(!this.userDetailsImpl.equals(null), "userDetailsImpl object mustn't be equal to a null object");
    }
    @Test
    public void testEquals4() {
        User user = new User();
        Assert.isTrue(!this.userDetailsImpl.equals(user), "userDetailsImpl object mustn't be equal to a User object");
    }

    @Test
    public void testBuilderToString() {
        Assert.state(UserDetailsImpl.builder().toString().equals("UserDetailsImpl.UserDetailsImplBuilder(id=null, username=null, firstName=null, lastName=null, admin=null, password=null)"), "UserDetailsImpl Builder object toString must be equal to \"UserDetailsImpl.UserDetailsImplBuilder(id=null, username=null, firstName=null, lastName=null, admin=null, password=null)\"");
    }

    @Test
    public void testBuilder() {
        UserDetailsImpl builtUserDetailsImpl = UserDetailsImpl.builder().id(22L).username("pierre.leblond@live.fr").firstName("Pierre").lastName("Leblond").admin(true).password("Janvier2004").build();
        Assert.state(builtUserDetailsImpl.getId().equals(22L), "Built UserDetailsImpl id must be equal to 22 (Long)");
        Assert.state(builtUserDetailsImpl.getUsername().equals("pierre.leblond@live.fr"), "Built UserDetailsImpl username must be equal to \"pierre.leblond@live.fr\"");
        Assert.state(builtUserDetailsImpl.getFirstName().equals("Pierre"), "Built UserDetailsImpl first name must be equal to \"Pierre\"");
        Assert.state(builtUserDetailsImpl.getLastName().equals("Leblond"), "Built UserDetailsImpl last name must be equal to \"Leblond\"");
        Assert.isTrue(builtUserDetailsImpl.getAdmin(), "Built UserDetailsImpl admin must be equal to true");
        Assert.state(builtUserDetailsImpl.getPassword().equals("Janvier2004"), "Built UserDetailsImpl password must be equal to \"Janvier2004\"");
    }
}
