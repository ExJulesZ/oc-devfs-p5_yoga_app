package com.openclassrooms.starterjwt.unit.models;

import org.springframework.util.Assert;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.openclassrooms.starterjwt.models.Session;
import com.openclassrooms.starterjwt.models.Teacher;
import com.openclassrooms.starterjwt.models.User;

public class SessionTest {

    private LocalDateTime now;
    private Teacher teacher;
    private Session session;

    @BeforeEach
    public void setSession() {
        this.now = LocalDateTime.now();
        this.teacher = new Teacher(4L,"Martin","Laura",this.now,this.now);
        this.session = new Session(7L,"Session n°7",new Date(1699999999999L),"Ceci est la septième session.",this.teacher,new ArrayList<>(),this.now,this.now);
    }

    @Test
    public void testId() {
        Assert.state(this.session.getId().equals(7L), "Id must be equal to 7 (Long)");
    }

    @Test
    public void testName() {
        Assert.state(this.session.getName().equals("Session n°7"), "Name must be equal to \"Session n°7\"");
    }

    @Test
    public void testDate() {
        Assert.state(this.session.getDate().toString().equals("Tue Nov 14 23:13:19 CET 2023"), "Date must be equal to \"Tue Nov 14 23:13:19 CET 2023\"");
    }

    @Test
    public void testDescription() {
        Assert.state(this.session.getDescription().equals("Ceci est la septième session."), "Description must be equal to \"Ceci est la septième session.\"");
    }

    @Test
    public void testTeacher() {
        Assert.state(this.session.getTeacher().equals(this.teacher), "Teacher must be equal to this.teacher");
    }

    @Test
    public void testUsers() {
        Assert.isTrue(this.session.getUsers().size()==0, "Users size must be equal to 0");
    }

    @Test
    public void testCreatedAt() {
        Assert.state(this.session.getCreatedAt().toString().equals(this.now.toString()), "CreatedAt must be equal to this.now");
    }

    @Test
    public void testUpdatedAt() {
        Assert.state(this.session.getUpdatedAt().toString().equals(this.now.toString()), "UpdatedAt must be equal to this.now");
    }

    @Test
    public void testNewId() {
        this.session.setId(8L);
        Assert.state(this.session.getId().equals(8L), "Now, id must be equal to 8 (Long)");
    }

    @Test
    public void testNewName() {
        this.session.setName("La session n°8");
        Assert.state(this.session.getName().equals("La session n°8"), "Now, name must be equal to \"La session n°8\"");
    }

    @Test
    public void testNewDate() {
        this.session.setDate(new Date(1710000000000L));
        Assert.state(this.session.getDate().toString().equals("Sat Mar 09 17:00:00 CET 2024"), "Now, date must be equal to \"Sat Mar 09 17:00:00 CET 2024\"");
    }

    @Test
    public void testNewDescription() {
        this.session.setDescription("La huitième session de yoga est disponible.");
        Assert.state(this.session.getDescription().equals("La huitième session de yoga est disponible."), "Now, description must be equal to \"La huitième session de yoga est disponible.\"");
    }

    @Test
    public void testNewTeacher() {
        Teacher newTeacher = new Teacher(12L,"Durand","Emma",this.now,this.now);
        this.session.setTeacher(newTeacher);
        Assert.state(this.session.getTeacher().equals(newTeacher), "Now, teacher must be equal to the new teacher");
    }

    @Test
    public void testNewUsers() {
        ArrayList<User> users = new ArrayList<>();
        users.add(new User());
        this.session.setUsers(users);
        Assert.isTrue(this.session.getUsers().size()==1, "Now, users size must be equal to 1");
    }

    @Test
    public void testNewCreatedAt() {
        this.session.setCreatedAt(this.now);
        Assert.state(this.session.getCreatedAt().toString().equals(this.now.toString()), "Now, createdAt must be equal to this.now (another date time because current)");
    }

    @Test
    public void testNewUpdatedAt() {
        this.session.setUpdatedAt(this.now);
        Assert.state(this.session.getUpdatedAt().toString().equals(this.now.toString()), "Now, updatedAt must be equal to this.now (another date time because current)");
    }

    @Test
    public void testEquals1() {
        Assert.isTrue(this.session.equals(this.session), "session object must be equal to himself");
    }
    @Test
    public void testEquals2() {
        Session session2 = new Session(10L,"Session n°10",new Date(1719199199999L),"C'est la dixième session !",this.teacher,new ArrayList<>(),this.now,this.now);
        Assert.isTrue(!this.session.equals(session2), "session object mustn't be equal to an another object (session2)");
    }
    @Test
    public void testEquals3() {
        Assert.isTrue(!this.session.equals(null), "session object mustn't be equal to a null object");
    }

    @Test
    public void testHashCode() {
        Assert.isTrue(this.session.hashCode()==66, "HashCode of session object must be equal to 66");
    }

    @Test
    public void testToString() {
        String now = this.now.toString();
        Assert.state(this.session.toString().equals("Session(id=7, name=Session n°7, date=Tue Nov 14 23:13:19 CET 2023, description=Ceci est la septième session., teacher=Teacher(id=4, lastName=Martin, firstName=Laura, createdAt="+now+", updatedAt="+now+"), users=[], createdAt="+now+", updatedAt="+now+")"), "session object toString must be equal to \"Session(id=7, name=Session n°7, date=Tue Nov 14 23:13:19 CET 2023, description=Ceci est la septième session., teacher=Teacher(id=4, lastName=Martin, firstName=Laura, createdAt="+now+", updatedAt="+now+"), users=[], createdAt="+now+", updatedAt="+now+")\"");
    }

    @Test
    public void testBuilderToString() {
        Assert.state(Session.builder().toString().equals("Session.SessionBuilder(id=null, name=null, date=null, description=null, teacher=null, users=null, createdAt=null, updatedAt=null)"), "Session Builder object toString must be equal to \"Session.SessionBuilder(id=null, name=null, date=null, description=null, teacher=null, users=null, createdAt=null, updatedAt=null)\"");
    }

    @Test
    public void testBuilderNull() {
        Session builtSession = Session.builder().build();
        Assert.isNull(builtSession.getId(), "Built Session id must be null");
        Assert.isNull(builtSession.getName(), "Built Session last name must be null");
        Assert.isNull(builtSession.getDate(), "Built Session first name must be null");
        Assert.isNull(builtSession.getDescription(), "Built Session last name must be null");
        Assert.isNull(builtSession.getTeacher(), "Built Session first name must be null");
        Assert.isNull(builtSession.getUsers(), "Built Session first name must be null");
        Assert.isNull(builtSession.getCreatedAt(), "Built Session createdAt must be null");
        Assert.isNull(builtSession.getUpdatedAt(), "Built Session updatedAt must be null");
    }

    @Test
    public void testBuilder() {
        Session builtSession = Session.builder().id(12L).name("Session 12").date(new Date(1710999955555L)).description("Session de yoga n°12").teacher(this.teacher).users(new ArrayList<>()).createdAt(this.now).updatedAt(this.now).build();
        Assert.state(builtSession.getId().equals(12L), "Built Session id must be equal to 12 (Long)");
        Assert.state(builtSession.getName().equals("Session 12"), "Built Session name must be equal to \"Session 12\"");
        Assert.state(builtSession.getDate().toString().equals("Thu Mar 21 06:45:55 CET 2024"), "Built Session date must be equal to \"Thu Mar 21 06:45:55 CET 2024\"");
        Assert.state(builtSession.getDescription().equals("Session de yoga n°12"), "Built Session description must be equal to \"Session de yoga n°12\"");
        Assert.state(builtSession.getTeacher().getId().equals(4L), "Built Session teacher id must be equal to 4 (Long)");
        Assert.isTrue(builtSession.getUsers().size()==0, "Built Session users size must be equal to 0");
        Assert.state(builtSession.getCreatedAt().equals(this.now), "Built Session createdAt must be equal to this.now");
        Assert.state(builtSession.getUpdatedAt().equals(this.now), "Built Session updatedAt must be equal to this.now");
    }
}
