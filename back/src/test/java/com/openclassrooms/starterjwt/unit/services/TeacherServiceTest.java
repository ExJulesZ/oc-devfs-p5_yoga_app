package com.openclassrooms.starterjwt.unit.services;

import static org.mockito.Mockito.*;

import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.util.Assert;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;

import com.openclassrooms.starterjwt.models.Teacher;
import com.openclassrooms.starterjwt.repository.TeacherRepository;
import com.openclassrooms.starterjwt.services.TeacherService;

@ExtendWith(MockitoExtension.class)
public class TeacherServiceTest {

    @Mock
    private TeacherRepository teacherRepository;

    private TeacherService teacherService;

    private LocalDateTime now;
    private Teacher teacher;

    @BeforeEach
    public void init() {
        this.teacherService = new TeacherService(this.teacherRepository);
        this.now = LocalDateTime.now();
        this.teacher = new Teacher(5L,"Thiercelin","Hélène",this.now,this.now);
    }

    @Test
    public void testFindAll() {
        // GIVEN
        Teacher teacher2 = new Teacher(7L,"Delahaye","Margot",this.now,this.now);;
        List<Teacher> teachers = new ArrayList<>();
        teachers.add(this.teacher);
        teachers.add(teacher2);
        when(this.teacherRepository.findAll()).thenReturn(teachers);

        // WHEN
        final List<Teacher> res = this.teacherService.findAll();

        // THEN
        Assert.isTrue(res.size()==2, "Teachers list size must be equal to 2");
        Assert.state(res.get(0).equals(this.teacher), "Teachers list first element must be equal to this.teacher");
        Assert.state(res.get(1).equals(teacher2), "Teachers list second element must be equal to teacher2");
        Assert.state(res.get(0).getId().equals(this.teacher.getId()), "Teachers list first element id must be equal to this.teacher id");
        Assert.state(res.get(1).getId().equals(teacher2.getId()), "Teachers list second element id must be equal to session2 id");
    }

    @Test
    public void testFindById() {
        // GIVEN
        final Long id = this.teacher.getId();
        when(this.teacherRepository.findById(id)).thenReturn(Optional.of(this.teacher));

        // WHEN
        final Teacher res = this.teacherService.findById(id);

        // THEN
        Assert.state(res.equals(this.teacher), "Found teacher must be equal to this.session");
        Assert.state(res.getId().equals(this.teacher.getId()), "Found teacher id must be equal to this.teacher id");
    }

    @Test
    public void testFindByIdNull() {
        // GIVEN
        final Long id = this.teacher.getId();
        when(this.teacherRepository.findById(id)).thenReturn(Optional.empty());

        // WHEN
        final Teacher res = this.teacherService.findById(id);

        // THEN
        Assert.isNull(res, "Found teacher must be null");
    }
}
