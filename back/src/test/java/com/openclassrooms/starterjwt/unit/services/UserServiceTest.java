package com.openclassrooms.starterjwt.unit.services;

import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;

import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.util.Assert;

import java.time.LocalDateTime;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;

import com.openclassrooms.starterjwt.models.User;
import com.openclassrooms.starterjwt.repository.UserRepository;
import com.openclassrooms.starterjwt.services.UserService;

@ExtendWith(MockitoExtension.class)
public class UserServiceTest {

    @Mock
    private UserRepository userRepository;

    private UserService userService;

    private User user;

    @BeforeEach
    public void init() {
        this.userService = new UserService(this.userRepository);
        LocalDateTime now = LocalDateTime.now();
        this.user = new User(1L,"jules.grelet@live.fr","Grelet","Jules","passwd000",true,now,now);
    }

    @Test
    public void testDelete() {
        // GIVEN
        final Long id = this.user.getId();
        doNothing().when(this.userRepository).deleteById(id);

        // WHEN
        this.userService.delete(id);

        // THEN
        verify(this.userRepository,times(1)).deleteById(id);
    }

    @Test
    public void testFindById() {
        // GIVEN
        final Long id = this.user.getId();
        when(this.userRepository.findById(id)).thenReturn(Optional.of(this.user));

        // WHEN
        final User res = this.userService.findById(id);

        // THEN
        Assert.state(res.equals(this.user), "Found user must be equal to this.user");
        Assert.state(res.getId().equals(this.user.getId()), "Found user id must be equal to this.user id");
    }

    @Test
    public void testFindByIdNull() {
        // GIVEN
        final Long id = this.user.getId();
        when(this.userRepository.findById(id)).thenReturn(Optional.empty());

        // WHEN
        final User res = this.userService.findById(id);

        // THEN
        Assert.isNull(res, "Found user must be null");
    }
}
