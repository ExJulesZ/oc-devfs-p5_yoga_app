package com.openclassrooms.starterjwt.unit.payload;

import org.springframework.util.Assert;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.openclassrooms.starterjwt.models.User;

import com.openclassrooms.starterjwt.payload.request.SignupRequest;

public class SignupRequestTest {

    private SignupRequest signupRequest;

    @BeforeEach
    public void setSignup() {
        this.signupRequest = new SignupRequest();
        this.signupRequest.setEmail("jeandupont@mail.com");
        this.signupRequest.setFirstName("Jean");
        this.signupRequest.setLastName("Dupont");
        this.signupRequest.setPassword("Password!123");
    }

    @Test
    public void testEmail() {
        Assert.state(this.signupRequest.getEmail().equals("jeandupont@mail.com"), "Email must be equal to \"jeandupont@mail.com\"");
    }

    @Test
    public void testFirstName() {
        Assert.state(this.signupRequest.getFirstName().equals("Jean"), "First name must be equal to \"Jean\"");
    }

    @Test
    public void testLastName() {
        Assert.state(this.signupRequest.getLastName().equals("Dupont"), "Last name must be equal to \"Dupont\"");
    }

    @Test
    public void testPassword() {
        Assert.state(this.signupRequest.getPassword().equals("Password!123"), "Password must be equal to \"Password!123\"");
    }

    @Test
    public void testNewEmail() {
        this.signupRequest.setEmail("jules.grelet@live.fr");
        Assert.state(this.signupRequest.getEmail().equals("jules.grelet@live.fr"), "Now, email must be equal to \"jules.grelet@live.fr\"");
    }

    @Test
    public void testNewFirstName() {
        this.signupRequest.setFirstName("Jules");
        Assert.state(this.signupRequest.getFirstName().equals("Jules"), "Now, first name must be equal to \"Jules\"");
    }

    @Test
    public void testNewLastName() {
        this.signupRequest.setLastName("Grelet");
        Assert.state(this.signupRequest.getLastName().equals("Grelet"), "Now, last name must be equal to \"Grelet\"");
    }

    @Test
    public void testNewPassword() {
        this.signupRequest.setPassword("Azerty!28");
        Assert.state(this.signupRequest.getPassword().equals("Azerty!28"), "Now, password must be equal to \"Azerty!28\"");
    }

    @Test
    public void testEquals1() {
        Assert.isTrue(this.signupRequest.equals(this.signupRequest), "signupRequest object must be equal to himself");
    }
    @Test
    public void testEquals2() {
        SignupRequest signupRequest2 = new SignupRequest();
        signupRequest2.setEmail("jules.grelet@live.fr");
        signupRequest2.setFirstName("Jules");
        signupRequest2.setLastName("Grelet");
        signupRequest2.setPassword("Azerty!28");
        Assert.isTrue(!this.signupRequest.equals(signupRequest2), "signupRequest object mustn't be equal to another object (signupRequest2)");
    }
    @Test
    public void testEquals3() {
        Assert.isTrue(!this.signupRequest.equals(null), "signupRequest object mustn't be equal to a null object");
    }
    @Test
    public void testEquals4() {
        User user = new User();
        Assert.isTrue(!this.signupRequest.equals(user), "signupRequest object mustn't be equal to a User object");
    }

    @Test
    public void testHashCode() {
        Assert.isTrue(this.signupRequest.hashCode()==-272424901, "HashCode of signupRequest object must be equal to -272424901 (int)");
    }

    @Test
    public void testToString() {
        Assert.state(this.signupRequest.toString().equals("SignupRequest(email=jeandupont@mail.com, firstName=Jean, lastName=Dupont, password=Password!123)"), "signupRequest object toString must be equal to \"SignupRequest(email=jeandupont@mail.com, firstName=Jean, lastName=Dupont, password=Password!123)\"");
    }
}
