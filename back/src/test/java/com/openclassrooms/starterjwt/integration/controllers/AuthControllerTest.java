package com.openclassrooms.starterjwt.integration.controllers;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

import org.json.JSONObject;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.util.Assert;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
@AutoConfigureMockMvc
public class AuthControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testAuthenticateUserOK() throws Exception {
        this.mockMvc.perform(post("/api/auth/login")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{ \"email\":\"yoga@studio.com\",\"password\":\"test!1234\" }")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.token").isNotEmpty())
                .andExpect(jsonPath("$.id").value(1L))
                .andExpect(jsonPath("$.username").value("yoga@studio.com"))
                .andExpect(jsonPath("$.lastName").value("Admin"))
                .andExpect(jsonPath("$.firstName").value("Admin"))
                .andExpect(jsonPath("$.admin").value(true));
    }

    @Test
    public void testAuthenticateUserBadCredentials() throws Exception {
        this.mockMvc.perform(post("/api/auth/login")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{ \"email\":\"yoga@studio.com\",\"password\":\"myPassword\" }")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void testRegisterUserOK() throws Exception {
        MvcResult mvcResult = this.mockMvc.perform(post("/api/auth/register")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{ \"email\":\"jean.dupont@live.fr\",\"firstName\":\"Jean\",\"lastName\":\"DUPONT\",\"password\":\"Dupont1234\" }")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        JSONObject res = new JSONObject(mvcResult.getResponse().getContentAsString());
        String message = res.getString("message");
        Assert.state(message.equals("User registered successfully!"), "On status OK, message response must be equal to \"User registered successfully!\"");
    }

    @Test
    public void testRegisterUserKO() throws Exception {
        MvcResult mvcResult = this.mockMvc.perform(post("/api/auth/register")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{ \"email\":\"jules.grelet@live.fr\",\"firstName\":\"Jules\",\"lastName\":\"Grelet\",\"password\":\"Password0000\" }")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn();

        JSONObject res = new JSONObject(mvcResult.getResponse().getContentAsString());
        String message = res.getString("message");
        Assert.state(message.equals("Error: Email is already taken!"), "On status KO (bad request), message response must be equal to \"Error: Email is already taken!\"");
    }
}
