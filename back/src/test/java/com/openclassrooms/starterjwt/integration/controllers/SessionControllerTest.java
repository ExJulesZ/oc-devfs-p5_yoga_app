package com.openclassrooms.starterjwt.integration.controllers;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.json.JSONObject;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.time.LocalDateTime;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
@AutoConfigureMockMvc
public class SessionControllerTest {

    @Autowired
    private MockMvc mockMvc;

    private String token;

    @BeforeEach
    public void init() throws Exception {
        MvcResult mvcResult = this.mockMvc.perform(post("/api/auth/login")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{ \"email\":\"yoga@studio.com\",\"password\":\"test!1234\" }")
                        .accept(MediaType.APPLICATION_JSON))
                .andReturn();
        JSONObject res = new JSONObject(mvcResult.getResponse().getContentAsString());
        this.token = res.getString("token");
    }

    @Test
    public void testFindByIdOK() throws Exception {
        mockMvc.perform(get("/api/session/2")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header(HttpHeaders.AUTHORIZATION, "Bearer "+this.token)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(2L))
                .andExpect(jsonPath("$.name").value("Session 2"))
                .andExpect(jsonPath("$.teacher_id").value(1L))
                .andExpect(jsonPath("$.description").value("La deuxième session de yoga de Margot."))
                .andExpect(jsonPath("$.users").isEmpty());
    }

    @Test
    public void testFindByIdUnauthorized() throws Exception {
        mockMvc.perform(get("/api/session/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void testFindByIdBadRequest() throws Exception {
        mockMvc.perform(get("/api/session/TEST")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header(HttpHeaders.AUTHORIZATION, "Bearer "+this.token)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testFindByIdNotFound() throws Exception {
        mockMvc.perform(get("/api/session/50")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header(HttpHeaders.AUTHORIZATION, "Bearer "+this.token)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testFindAllOK() throws Exception {
        mockMvc.perform(get("/api/session")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header(HttpHeaders.AUTHORIZATION, "Bearer "+this.token)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$[0]").exists())
                .andExpect(jsonPath("$[0]").isNotEmpty())
                .andExpect(jsonPath("$[0].id").value(1L))
                .andExpect(jsonPath("$[0].name").value("Session 1"))
                .andExpect(jsonPath("$[1]").exists())
                .andExpect(jsonPath("$[1]").isNotEmpty())
                .andExpect(jsonPath("$[1].id").value(2L))
                .andExpect(jsonPath("$[1].name").value("Session 2"))
                .andExpect(jsonPath("$[2]").exists())
                .andExpect(jsonPath("$[2]").isNotEmpty())
                .andExpect(jsonPath("$[2].id").value(3L))
                .andExpect(jsonPath("$[2].name").value("Session 3"));
    }

    @Test
    public void testFindAllUnauthorized() throws Exception {
        mockMvc.perform(get("/api/session")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void testCreateOK() throws Exception {
        LocalDateTime now = LocalDateTime.now();
        mockMvc.perform(post("/api/session")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header(HttpHeaders.AUTHORIZATION, "Bearer "+this.token)
                        .content("{ \"id\":4," +
                                "\"name\":\"Session 4\"," +
                                "\"date\":\""+now+"\"," +
                                "\"teacher_id\":1," +
                                "\"description\":\"C'est la quatrième session de yoga.\"," +
                                "\"users\":[]," +
                                "\"createdAt\":\""+now+"\"," +
                                "\"updatedAt\":\""+now+"\" }")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(4L))
                .andExpect(jsonPath("$.name").value("Session 4"))
                .andExpect(jsonPath("$.teacher_id").value(1L))
                .andExpect(jsonPath("$.description").value("C'est la quatrième session de yoga."))
                .andExpect(jsonPath("$.users").isEmpty());
    }

    @Test
    public void testCreateUnauthorized() throws Exception {
        LocalDateTime now = LocalDateTime.now();
        mockMvc.perform(post("/api/session")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{ \"id\":1," +
                                "\"name\":\"Session 1\"," +
                                "\"date\":\""+now+"\"," +
                                "\"teacher_id\":1," +
                                "\"description\":\"C'est la première session de yoga.\"," +
                                "\"users\":[]," +
                                "\"createdAt\":\""+now+"\"," +
                                "\"updatedAt\":\""+now+"\" }")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void testUpdateOK() throws Exception {
        LocalDateTime now = LocalDateTime.now();
        mockMvc.perform(put("/api/session/2")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header(HttpHeaders.AUTHORIZATION, "Bearer "+this.token)
                        .content("{ \"id\":2," +
                                "\"name\":\"Session n°2\"," +
                                "\"date\":\""+now+"\"," +
                                "\"teacher_id\":2," +
                                "\"description\":\"Ceci est la deuxième session de yoga du club !\"," +
                                "\"users\":[]," +
                                "\"createdAt\":\""+now+"\"," +
                                "\"updatedAt\":\""+now+"\" }")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(2L))
                .andExpect(jsonPath("$.name").value("Session n°2"))
                .andExpect(jsonPath("$.teacher_id").value(2L))
                .andExpect(jsonPath("$.description").value("Ceci est la deuxième session de yoga du club !"))
                .andExpect(jsonPath("$.users").isArray())
                .andExpect(jsonPath("$.users").isEmpty());
    }

    @Test
    public void testUpdateUnauthorized() throws Exception {
        LocalDateTime now = LocalDateTime.now();
        mockMvc.perform(put("/api/session/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{ \"id\":1," +
                                "\"name\":\"Session n°1\"," +
                                "\"date\":\""+now+"\"," +
                                "\"teacher_id\":2," +
                                "\"description\":\"Ceci est la toute première session de yoga du club.\"," +
                                "\"users\":[]," +
                                "\"createdAt\":\""+now+"\"," +
                                "\"updatedAt\":\""+now+"\" }")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void testUpdateBadRequest() throws Exception {
        LocalDateTime now = LocalDateTime.now();
        mockMvc.perform(put("/api/session/TEST")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header(HttpHeaders.AUTHORIZATION, "Bearer "+this.token)
                        .content("{ \"id\":1," +
                                "\"name\":\"Session n°1\"," +
                                "\"date\":\""+now+"\"," +
                                "\"teacher_id\":2," +
                                "\"description\":\"Ceci est la toute première session de yoga du club.\"," +
                                "\"users\":[]," +
                                "\"createdAt\":\""+now+"\"," +
                                "\"updatedAt\":\""+now+"\" }")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testDeleteOK() throws Exception {
        mockMvc.perform(delete("/api/session/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header(HttpHeaders.AUTHORIZATION, "Bearer "+this.token)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void testDeleteUnauthorized() throws Exception {
        mockMvc.perform(delete("/api/session/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void testDeleteBadRequest() throws Exception {
        mockMvc.perform(delete("/api/session/TEST")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header(HttpHeaders.AUTHORIZATION, "Bearer "+this.token)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testDeleteNotFound() throws Exception {
        mockMvc.perform(delete("/api/session/50")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header(HttpHeaders.AUTHORIZATION, "Bearer "+this.token)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testParticipate() throws Exception {
        mockMvc.perform(post("/api/session/1/participate/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header(HttpHeaders.AUTHORIZATION, "Bearer "+this.token)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void testParticipateUnauthorized() throws Exception {
        mockMvc.perform(post("/api/session/1/participate/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void testParticipateBadRequest() throws Exception {
        mockMvc.perform(post("/api/session/TEST/participate/TEST")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header(HttpHeaders.AUTHORIZATION, "Bearer "+this.token)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testNoLongerParticipate() throws Exception {
        mockMvc.perform(delete("/api/session/1/participate/2")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header(HttpHeaders.AUTHORIZATION, "Bearer "+this.token)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void testNoLongerParticipateUnauthorized() throws Exception {
        mockMvc.perform(delete("/api/session/1/participate/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void testNoLongerParticipateBadRequest() throws Exception {
        this.mockMvc.perform(delete("/api/session/1/participate/TEST")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header(HttpHeaders.AUTHORIZATION, "Bearer "+this.token)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }
}
