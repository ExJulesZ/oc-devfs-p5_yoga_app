INSERT INTO `TEACHERS` (first_name, last_name)
VALUES ('Margot', 'DELAHAYE'),
       ('Hélène', 'THIERCELIN');

INSERT INTO `USERS` (first_name, last_name, admin, email, password)
VALUES ('Admin', 'Admin', true, 'yoga@studio.com', '$2a$10$.Hsa/ZjUVaHqi0tp9xieMeewrnZxrZ5pQRzddUXE/WjDu2ZThe6Iq'),
       ('Jules', 'Grelet', false, 'jules.grelet@live.fr', '$2y$10$aDUoaMTZOMrNYVge07.cWORHLAmYKHAlgn6Cljao02YU09vC8tLNi');

INSERT INTO `SESSIONS` (name,description,date,teacher_id)
VALUES ('Session 1','La première session de yoga de Margot.','2023-09-29 20:00:00',1),
       ('Session 2','La deuxième session de yoga de Margot.','2023-10-12 09:00:00',1),
       ('Session 3','La session de yoga d Hélène.','2023-09-29 15:00:00',2);

INSERT INTO `PARTICIPATE` (user_id,session_id)
VALUES (2,1);