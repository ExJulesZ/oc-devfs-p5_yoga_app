# P5 : Yoga_App Tests



## I - Mise en place de l’application


### 1. Récupérez le projet avec `git clone https://gitlab.com/ExJulesZ/oc-devfs-p5_yoga_app.git`.


### 2. Installation de la base de données

1. Démarrez Wamp (ou équivalent) ;

2. Dans phpMyAdmin ou en ligne de commande, créez une nouvelle base de données nommée **test** ;

3. Utilisez le script sql fourni (**oc-devfs-p5_yoga_app/ressources/sql/script.sql**) contenant le schéma et des données de départ.


### 3. Installation de l’application

1. Ouvrez le répertoire **back** avec IntelliJ (ou autre IDE) ;

2. Le nécessaire sera téléchargé automatiquement, mais si besoin ouvrez **pom.xml** et cliquez sur le bouton **Load Maven Changes** ou faites **Ctrl+Maj+O** ;

3. Accédez à **front** avec `cd front` ;

4. Installez toutes les dépendances nécessaires avec `npm i`.


### 4. Lancement de l’application

1. Dans IntelliJ, faites un click-droit sur **SpringBootSecurityJwtApplication.java** et cliquez sur **Run ...** ;

2. Au premier lancement l'application back-end doit build. Dans ce cas recommencez le point précédent ;

3. Accédez à **front** avec `cd front` ;

4. Démarrez l'application front-end avec `npm run start` ;

5. Une fois l'application lancée, suivez le lien indiqué. Cela pourrait être  [http://localhost:4200/]( http://localhost:4200/) ;

6. Pour se connecter en admin : cliquez sur **Login**, entrez **yoga@studio.com** et **test!1234**, et cliquez sur **Submit**.



## II - Tests de l’application

Trois différentes séries de tests ont été effectuées :
- tests front-end unitaires et d'intégration → 81,8 % de couverture ;
- tests end-to-end → 94,5 % de couverture ;
- tests back-end → 92 % de couverture.


### 1. Lancement des tests

1. Accédez à **front** avec `cd front` ;

2. Lancez les tests front-end avec `npm run test` ;

3. Lancez les tests end-to-end avec `npm run e2e`. Après compilation, une fenêtre Cypress s'ouvre ;

4. Dans la fenêtre, cliquez sur le navigateur web de votre choix parmi ceux proposés et cliquez sur le bouton **Start E2E Testing** ;

5. Cliquez sur les tests listés dans l'écran affiché pour les lancer. Le test **Complete** permet de lancer tous les tests en une seule fois ;

6. Cliquez sur le bouton **Specs**, deuxième icône de la barre latérale de gauche, pour revenir à l'écran précédent ;

7. Dans IntelliJ (répertoire **back**), ouvrez **/src/test/java/com.openclassrooms.starterjwt/** ;

8. Pour chaque test, faites un click-droit sur son fichier et cliquez sur **Run ...Test**.


### 2. Génération des rapports de couverture

1. Accédez à **front** avec `cd front` ;

2. Lancez les tests front-end et la génération du rapport de couverture avec `"node_modules/.bin/jest" --coverage` ;

3. Ouvrez **/front/coverage/jest/lcov-report/index.html** pour consulter le rapport de couverture des tests front-end ;

4. Lancez les tests end-to-end avec `npm run e2e` ;

5. Sélectionnez un navigateur, cliquez sur le bouton **Start E2E Testing** et lancez le test **Complete** ;

6. Dans un autre cmd, accédez à **front** avec `cd front` ;

7. Lancez la génération du rapport de couverture avec `npm run e2e:coverage` ;

8. Ouvrez **/front/coverage/lcov-report/index.html** pour consulter le rapport de couverture des tests end-to-end ;

9. Accédez à **back** avec `cd ..` et `cd back` ;

10. Lancez les tests back-end et la génération du rapport de couverture avec `mvn clean test` ;

11. Ouvrez **back/target/site/jacoco/index.html** pour consulter le rapport de couverture des tests back-end.
